�
2(�Sc           @   s�   d  Z  d d l m Z d d l m Z d d l m Z d d l m Z d g Z	 d e f d �  �  YZ
 d �  Z e d	 k r� e �  n  d
 S(   t   restructuredtexti����(   t   numerix(   t   MA(   t   getsetDeprecated(   t   CellVariablet   DistanceVariablec           B   s7  e  Z d  Z d d d d d d � Z d �  Z e d � Z d e d � Z d d e d	 � Z	 d
 �  Z
 e d �  � Z e d �  � Z e d �  � Z e d �  � Z e d �  � Z e d �  � Z e d �  � Z e d �  � Z e d �  � Z e d �  � Z e d �  � Z e d �  � Z e d �  � Z e d �  � Z RS(   sn  
    A `DistanceVariable` object calculates :math:`\phi` so it satisfies,

    .. math::
        
       \abs{\nabla \phi} = 1

    using the fast marching method with an initial condition defined by
    the zero level set.

    Currently the solution is first order, This suffices for initial
    conditions with straight edges (e.g. trenches in
    electrodeposition). The method should work for unstructured 2D grids
    but testing on unstructured grids is untested thus far. This is a 2D
    implementation as it stands. Extending to 3D should be relatively
    simple.

    Here we will define a few test cases. Firstly a 1D test case

    >>> from fipy.meshes import Grid1D
    >>> from fipy.tools import serial
    >>> mesh = Grid1D(dx = .5, nx = 8, communicator=serial)
    >>> from distanceVariable import DistanceVariable
    >>> var = DistanceVariable(mesh = mesh, value = (-1, -1, -1, -1, 1, 1, 1, 1))
    >>> var.calcDistanceFunction()
    >>> answer = (-1.75, -1.25, -.75, -0.25, 0.25, 0.75, 1.25, 1.75)
    >>> print var.allclose(answer)
    1

    A 1D test case with very small dimensions.

    >>> dx = 1e-10
    >>> mesh = Grid1D(dx = dx, nx = 8, communicator=serial)
    >>> var = DistanceVariable(mesh = mesh, value = (-1, -1, -1, -1, 1, 1, 1, 1))
    >>> var.calcDistanceFunction()
    >>> answer = numerix.arange(8) * dx - 3.5 * dx
    >>> print var.allclose(answer)
    1

    A 2D test case to test `_calcTrialValue` for a pathological case.

    >>> dx = 1.
    >>> dy = 2.
    >>> from fipy.meshes import Grid2D
    >>> mesh = Grid2D(dx = dx, dy = dy, nx = 2, ny = 3)
    >>> var = DistanceVariable(mesh = mesh, value = (-1, 1, 1, 1, -1, 1))

    >>> var.calcDistanceFunction()
    >>> vbl = -dx * dy / numerix.sqrt(dx**2 + dy**2) / 2.
    >>> vbr = dx / 2
    >>> vml = dy / 2.
    >>> crossProd = dx * dy
    >>> dsq = dx**2 + dy**2
    >>> top = vbr * dx**2 + vml * dy**2
    >>> sqrt = crossProd**2 *(dsq - (vbr - vml)**2)
    >>> sqrt = numerix.sqrt(max(sqrt, 0))
    >>> vmr = (top + sqrt) / dsq
    >>> answer = (vbl, vbr, vml, vmr, vbl, vbr)
    >>> print var.allclose(answer)
    1

    The `extendVariable` method solves the following equation for a given
    extensionVariable.

    .. math::

       \nabla u \cdot \nabla \phi = 0

    using the fast marching method with an initial condition defined at
    the zero level set. Essentially the equation solves a fake distance
    function to march out the velocity from the interface.

    >>> from fipy.variables.cellVariable import CellVariable
    >>> mesh = Grid2D(dx = 1., dy = 1., nx = 2, ny = 2)
    >>> var = DistanceVariable(mesh = mesh, value = (-1, 1, 1, 1))
    >>> var.calcDistanceFunction()
    >>> extensionVar = CellVariable(mesh = mesh, value = (-1, .5, 2, -1))
    >>> tmp = 1 / numerix.sqrt(2)
    >>> print var.allclose((-tmp / 2, 0.5, 0.5, 0.5 + tmp))
    1
    >>> var.extendVariable(extensionVar)
    >>> print extensionVar.allclose((1.25, .5, 2, 1.25))
    1
    >>> mesh = Grid2D(dx = 1., dy = 1., nx = 3, ny = 3)
    >>> var = DistanceVariable(mesh = mesh, value = (-1, 1, 1,
    ...                                               1, 1, 1,
    ...                                               1, 1, 1))
    >>> var.calcDistanceFunction()
    >>> extensionVar = CellVariable(mesh = mesh, value = (-1, .5, -1,
    ...                                                    2, -1, -1,
    ...                                                   -1, -1, -1))

    >>> v1 = 0.5 + tmp
    >>> v2 = 1.5
    >>> tmp1 = (v1 + v2) / 2 + numerix.sqrt(2. - (v1 - v2)**2) / 2
    >>> tmp2 = tmp1 + 1 / numerix.sqrt(2)
    >>> print var.allclose((-tmp / 2, 0.5, 1.5, 0.5, 0.5 + tmp, 
    ...                      tmp1, 1.5, tmp1, tmp2))
    1
    >>> answer = (1.25, .5, .5, 2, 1.25, 0.9544, 2, 1.5456, 1.25)
    >>> var.extendVariable(extensionVar)
    >>> print extensionVar.allclose(answer, rtol = 1e-4)
    1

    Test case for a bug that occurs when initializing the distance
    variable at the interface. Currently it is assumed that adjacent cells
    that are opposite sign neighbors have perpendicular normal vectors. In
    fact the two closest cells could have opposite normals.

    >>> mesh = Grid1D(dx = 1., nx = 3)
    >>> var = DistanceVariable(mesh = mesh, value = (-1, 1, -1))
    >>> var.calcDistanceFunction()
    >>> print var.allclose((-0.5, 0.5, -0.5))
    1

    For future reference, the minimum distance for the interface cells can
    be calculated with the following functions. The trial cell values will
    also be calculated with these functions. In essence it is not
    difficult to calculate the level set distance function on an
    unstructured 3D grid. However a lot of testing will be required. The
    minimum distance functions will take the following form.

    .. math::

       X_{\text{min}} = \frac{\left| \vec{s} \times \vec{t} \right|} {\left|
       \vec{s} - \vec{t} \right|}

    and in 3D,

    .. math::
        
       X_{\text{min}} = \frac{1}{3!} \left| \vec{s} \cdot \left( \vec{t} \times
       \vec{u} \right) \right|

    where the vectors :math:`\vec{s}`, :math:`\vec{t}` and :math:`\vec{u}` represent the
    vectors from the cell of interest to the neighboring cell.
    t    g        i    g    _�Bc         C   s�   t  j |  | d | d | d | d | �|  j �  | |  _ t j |  j j d � |  _ t j |  j j	 d � |  _
 t j |  j j d � |  _ t j |  j j � |  _ |  j j |  _ |  j j |  _ |  j j |  _ d S(   s�  
        Creates a `distanceVariable` object.

        :Parameters:
          - `mesh`: The mesh that defines the geometry of this variable.
          - `name`: The name of the variable.
	  - `value`: The initial value.
	  - `unit`: the physical units of the variable
          - `hasOld`: Whether the variable maintains an old value.
          - `narrowBandWidth`: The width of the region about the zero level set
            within which the distance function is evaluated.

        t   namet   valuet   unitt   hasOldi    N(   R   t   __init__t
   _markStalet   narrowBandWidthR   t   filledt   mesht   _cellToCellDistancest   cellToCellDistancest   _cellNormalst   cellNormalst
   _cellAreast	   cellAreasR   t   arrayt   _cellToCellIDsFilledt   cellToCellIDst   _adjacentCellIDst   adjacentCellIDst   exteriorFacest   cellFaceIDs(   t   selfR   R   R   R	   R
   R   (    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyR   �   s    (
	c         C   s   |  j  S(   N(   t   _value(   R   (    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyt
   _calcValue�   s    c         C   sK   |  j  j �  |  _ t j | � } |  j | d | �| | (|  j |  _  d S(   sp  
        
        Takes a `cellVariable` and extends the variable from the zero
        to the region encapuslated by the `narrowBandWidth`.

        :Parameters:
          - `extensionVariable`: The variable to extend from the zero
            level set.
          - `deleteIslands`: Sets the temporary level set value to
            zero in isolated cells.

        t   deleteIslandsN(   R   t   copyt   tmpValueR   R   t   _calcDistanceFunction(   R   t   extensionVariableR    t   numericExtensionVariable(    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyt   extendVariable�   s
    c         C   s$   |  j  d | d | � |  j �  d S(   sV  
        Calculates the `distanceVariable` as a distance function.

        :Parameters:
          - `narrowBandWidth`: The width of the region about the zero level set
            within which the distance function is evaluated.
          - `deleteIslands`: Sets the temporary level set value to
            zero in isolated cells.

        R   R    N(   R#   t
   _markFresh(   R   R   R    (    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyt   calcDistanceFunction�   s    c         C   su  | d  k r |  j } n  |  j j } | r� t j |  j | � } t j | d | |  j d k �} t j	 t j
 t j | � � d � } t j | d k |  j d k � } t j | d |  j � |  _ n  t j |  j | � } t j | d | |  j d k �} |  j j }	 t |  j |	 |  j | � }
 t j |
 d � } |  j d k d d } |
 | d t j | j d � f } |  j j d k r;|
 | d t j | j d � f } |
 | d t j | j d � f } | j d d k r6|  j d | d t j | j d � f } |  j d | d t j | j d � f } n@ t j |  j j d  d d � } t j |  j j d  d d � } t j t j | � |  j t j t j | � | | t j t t j | | � � d	 k  | | | t j | d | d � t j t j | � | | | | | t j | d | d � � � � � } n% t j t j | � |  j | | � } | |  _ t j	 t j
 t j |
 � � d � } | d k j d � } t } | d  k r�t j |  j j d
 � } t } n  t j |  j j d
 � } t j |  j d k | d � } t j t j |  j d k  | d � � d } x3 | D]+ } |  j | | | � \ } | d | f <q=W| r�|  j j �  |  _ n  t j | | � } t j	 t j  | d � d � d k j d � } t j t j
 | � | � j d � } t! t j | � d � } | } x: | D]2 } |  j | | | � \ |  j d | f <| | <qWxt" | � r[| t j# t t j |  j | � � � } t |  j d | f � | d k r�Pn  | j$ | � d | d | f <x� t j  | d | f d � D]v } | d k r�| d | f sT|  j | | | � \ |  j d | f <| d | f <| | k rQ| j% | � qQqTq�q�WqIWt j& |  j � |  _ d  S(   Nt   maski    i   i����i   i   .t   lg�������?t   d(   i    (   i    ('   t   NoneR   R   t   _cellToCellIDsR   t   takeR   R   t   masked_arrayt   sumt   logical_nott   getmaskt   logical_andt   whereR   t   abst   argsortt   aranget   shapet   dimR   t   zerost   dott   sqrtt   astypet   Truet   numberOfCellst   Falset   nonzerot   _calcTrialValueR"   R!   R   t   listt   lent   argmint   removet   appendR   (   R   R$   R   R    R   t   adjValst   adjInterfaceValuest   masksumt   tmpt   dAPt	   distancest   indicest   signt   st   tt   ut   nst   ntt   signedDistancet   interfaceFlagt   flagt   extt   positiveInterfaceFlagt   negativeInterfaceIDst   idt   adjInterfaceFlagt   hasAdjInterfacet	   trialFlagt   trialIDst   evaluatedFlagt   adjID(    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyR#   �   s�    "$!"$$$*-  !$6	$	+)-$0(##2c   !      C   s�  |  j  d | f } t j | | � } t j |  j | � } t j | | d � } y t j t | � � } Wn# t k
 r� t | � j �  } n X|  j | d k d d } |  j | d | f }	 |  j d | | d f }
 | d | | d f } t j	 | � } | d } | d } | |  j
 j } | d k r�|  j d | | f } |  j d | | f } |  j
 j d k r�| d | d | d | d } n d } t | � d k  r�| d k r�d } q�| d k r�| } q�q�n  | d k r�t � n�| d k r|
 | |	 | f S|  j | | f } |  j d | | f } |  j d | | f } |  j d | | f } |	 | | d | d | d | d } |	 | t j | | � } |	 d | d d | } |
 | | d | | |	 d } | d | |
 | d } t j t | d � � } | | | | } | d | | f } |  j | | f } |  j | | f } |  j | d k r�t | d � } n t | d � } | t |
 | � |	 } | t | | � | }  | | | | |  | |  f Sd  S(	   N.g    _�Bi    i   i   g        g�������?i   (   R   R   R.   R   R4   R6   R5   t	   TypeErrorR   R0   R   R9   R   t	   ExceptionR;   R<   t   maxR   t   min(!   R   R[   R`   R$   t   adjIDst   adjEvaluatedFlagt	   adjValuesRN   RO   t   d0t   v0t   e0t   Nt   index0t   index1t   index2t   n0t   n1t   crosst   d1t   v1t	   crossProdt   dotProdt   dsqt   topR<   t   dist   e1t   a0t   a1t   phit   n0gradt   n1grad(    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyRB   h  sf    

%		*#c         C   s   |  j  S(   N(   t   cellInterfaceAreas(   R   (    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyt   getCellInterfaceAreas�  s    c      	   C   sv   t  j t j |  j d � � } t  j t j |  j j d � � } t d |  j d t  j t	 t  j
 | | � � d d �� S(   s�  
        Returns the length of the interface that crosses the cell

        A simple 1D test:

        >>> from fipy.meshes import Grid1D
        >>> mesh = Grid1D(dx = 1., nx = 4)
        >>> distanceVariable = DistanceVariable(mesh = mesh, 
        ...                                     value = (-1.5, -0.5, 0.5, 1.5))
        >>> answer = CellVariable(mesh=mesh, value=(0, 0., 1., 0))
        >>> print numerix.allclose(distanceVariable.cellInterfaceAreas, 
        ...                        answer)
        True

        A 2D test case:
        
        >>> from fipy.meshes import Grid2D
        >>> from fipy.variables.cellVariable import CellVariable
        >>> mesh = Grid2D(dx = 1., dy = 1., nx = 3, ny = 3)
        >>> distanceVariable = DistanceVariable(mesh = mesh, 
        ...                                     value = (1.5, 0.5, 1.5,
        ...                                              0.5,-0.5, 0.5,
        ...                                              1.5, 0.5, 1.5))
        >>> answer = CellVariable(mesh=mesh,
        ...                       value=(0, 1, 0, 1, 0, 1, 0, 1, 0))
        >>> print numerix.allclose(distanceVariable.cellInterfaceAreas, answer)
        True

        Another 2D test case:

        >>> mesh = Grid2D(dx = .5, dy = .5, nx = 2, ny = 2)
        >>> from fipy.variables.cellVariable import CellVariable
        >>> distanceVariable = DistanceVariable(mesh = mesh, 
        ...                                     value = (-0.5, 0.5, 0.5, 1.5))
        >>> answer = CellVariable(mesh=mesh,
        ...                       value=(0, numerix.sqrt(2) / 4,  numerix.sqrt(2) / 4, 0))
        >>> print numerix.allclose(distanceVariable.cellInterfaceAreas, 
        ...                        answer)
        True

        Test to check that the circumfrence of a circle is, in fact, 
        :math:`2\pi r`.
	
        >>> mesh = Grid2D(dx = 0.05, dy = 0.05, nx = 20, ny = 20)
        >>> r = 0.25
        >>> x, y = mesh.cellCenters
        >>> rad = numerix.sqrt((x - .5)**2 + (y - .5)**2) - r
        >>> distanceVariable = DistanceVariable(mesh = mesh, value = rad)
        >>> print numerix.allclose(distanceVariable.cellInterfaceAreas.sum(), 1.57984690073)
        1
        i    R   R   t   axis(   R   R   R   R   t   _cellInterfaceNormalsR   t   _cellAreaProjectionsR   R0   R5   R;   (   R   t   normalst   areas(    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyR�   �  s    5!c         C   s   |  j  S(   N(   R�   (   R   (    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyt   _getCellInterfaceNormals�  s    c         C   s�   |  j  j } |  j  j } |  j  j } t j |  j t j d f | d d �} |  j j	 d d k r{ |  j
 d |  j f } n d } d d l m } | j | d k  d | � S(   s1  
        
        Returns the interface normals over the cells.

           >>> from fipy.meshes import Grid2D
           >>> from fipy.variables.cellVariable import CellVariable
           >>> mesh = Grid2D(dx = .5, dy = .5, nx = 2, ny = 2)
           >>> distanceVariable = DistanceVariable(mesh = mesh, 
           ...                                     value = (-0.5, 0.5, 0.5, 1.5))
           >>> v = 1 / numerix.sqrt(2)
           >>> answer = CellVariable(mesh=mesh,
           ...                       value=(((0, 0, v, 0),
           ...                               (0, 0, 0, 0),
           ...                               (0, 0, 0, 0),
           ...                               (0, v, 0, 0)),
           ...                              ((0, 0, v, 0),
           ...                               (0, 0, 0, 0),
           ...                               (0, 0, 0, 0),
           ...                               (0, v, 0, 0))))
           >>> print numerix.allclose(distanceVariable._cellInterfaceNormals, answer)
           True
           
        .R�   i    i����(   R   (   R   R?   t   _maxFacesPerCellR9   R   t   repeatt   _cellValueOverFacest   newaxisR   R8   t   _interfaceNormalst   fipy.tools.numerixR   R4   (   R   Rl   t   MR9   t   valueOverFacest   interfaceNormalsR   (    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyR�   �  s    (c         C   s   |  j  S(   N(   R�   (   R   (    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyt   _getInterfaceNormals  s    c         C   sJ   |  j  j } t j |  j t j d f | d d �} t j | |  j d � S(   s  

        Returns the normals on the boundary faces only, the other are set to zero.

           >>> from fipy.meshes import Grid2D
           >>> from fipy.variables.faceVariable import FaceVariable
           >>> mesh = Grid2D(dx = .5, dy = .5, nx = 2, ny = 2)
           >>> distanceVariable = DistanceVariable(mesh = mesh, 
           ...                                     value = (-0.5, 0.5, 0.5, 1.5))
           >>> v = 1 / numerix.sqrt(2)
           >>> answer = FaceVariable(mesh=mesh,
           ...                       value=((0, 0, v, 0, 0, 0, 0, v, 0, 0, 0, 0),
           ...                              (0, 0, v, 0, 0, 0, 0, v, 0, 0, 0, 0)))
           >>> print numerix.allclose(distanceVariable._interfaceNormals, answer)
           True
           
        .R�   i    (   R   R9   R   R�   t   _interfaceFlagR�   R4   t   _levelSetNormals(   R   R�   RV   (    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyR�     s    (c         C   s   |  j  S(   N(   R�   (   R   (    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyt   _getInterfaceFlag6  s    c         C   sj   |  j  } t j t j |  j � | d � } t j t j |  j � | d � } t j | | d k  d d � S(   st  

        Returns 1 for faces on boundary and 0 otherwise.

           >>> from fipy.meshes import Grid2D
           >>> from fipy.variables.faceVariable import FaceVariable
           >>> mesh = Grid2D(dx = .5, dy = .5, nx = 2, ny = 2)
           >>> distanceVariable = DistanceVariable(mesh = mesh, 
           ...                                     value = (-0.5, 0.5, 0.5, 1.5))
           >>> answer = FaceVariable(mesh=mesh,
           ...                       value=(0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0))
           >>> print numerix.allclose(distanceVariable._interfaceFlag, answer)
           True
           
        i    i   (   R   R   R.   R   R   R4   (   R   R   t   val0t   val1(    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyR�   :  s    	""c         C   s   |  j  S(   N(   t   _cellInterfaceFlag(   R   (    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyt   _getCellInterfaceFlagQ  s    c         C   sg   t  j t j |  j |  j � d � } t j | d d �} t j t j |  j	 d k | d k � d d � S(   s  

        Returns 1 for those cells on the interface:

        >>> from fipy.meshes import Grid2D
        >>> from fipy.variables.cellVariable import CellVariable
        >>> mesh = Grid2D(dx = .5, dy = .5, nx = 2, ny = 2)
        >>> distanceVariable = DistanceVariable(mesh = mesh, 
        ...                                     value = (-0.5, 0.5, 0.5, 1.5))
        >>> answer = CellVariable(mesh=mesh, value=(0, 1, 1, 0))
        >>> print numerix.allclose(distanceVariable._cellInterfaceFlag, answer)
        True

        i    R�   i   (
   R   R   R   R.   R�   R   R0   R4   R3   R   (   R   RW   (    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyR�   U  s    $c         C   s   |  j  S(   N(   R�   (   R   (    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyt   _getCellValueOverFacesk  s    c         C   sY   |  j  j } |  j  j } t j t j t j |  j � t j d f | d d �| | f � S(   s  

        Returns the cells values at the faces.

           >>> from fipy.meshes import Grid2D
           >>> from fipy.variables.cellVariable import CellVariable
           >>> mesh = Grid2D(dx = .5, dy = .5, nx = 2, ny = 2)
           >>> distanceVariable = DistanceVariable(mesh = mesh, 
           ...                                     value = (-0.5, 0.5, 0.5, 1.5))
           >>> answer = CellVariable(mesh=mesh,
           ...                       value=((-.5, .5, .5, 1.5),
           ...                              (-.5, .5, .5, 1.5),
           ...                              (-.5, .5, .5, 1.5),
           ...                              (-.5, .5, .5, 1.5)))
           >>> print numerix.allclose(distanceVariable._cellValueOverFaces, answer)
           True

        .R�   i    (	   R   R�   R?   R   t   reshapeR�   R   R   R�   (   R   R�   Rl   (    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyR�   o  s    c         C   s   |  j  S(   N(   R�   (   R   (    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyt   _getLevelSetNormals�  s    c         C   s�   |  j  j } t j | j � } t j | d k | d � } t j | � } |  j j } t |  j j � d k r� d | d |  j j f <n  | | S(   s�  

        Return the face level set normals.

           >>> from fipy.meshes import Grid2D
           >>> from fipy.variables.faceVariable import FaceVariable
           >>> mesh = Grid2D(dx = .5, dy = .5, nx = 2, ny = 2)
           >>> distanceVariable = DistanceVariable(mesh = mesh, 
           ...                                     value = (-0.5, 0.5, 0.5, 1.5))
           >>> v = 1 / numerix.sqrt(2)
           >>> answer = FaceVariable(mesh=mesh,
           ...                       value=((0, 0, v, v, 0, 0, 0, v, 0, 0, v, 0),
           ...                              (0, 0, v, v, 0, 0, 0, v, 0, 0, v, 0)))
           >>> print numerix.allclose(distanceVariable._levelSetNormals, answer)
           True
        g�����|�=i    g        .(	   t   gradt   arithmeticFaceValueR   R   t   magR4   R   R   RD   (   R   t   faceGradt   faceGradMagR   (    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyR�   �  s    	N(   t   __name__t
   __module__t   __doc__R,   R   R   R@   R&   R(   R#   RB   R   R�   t   propertyR�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   (    (    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyR   ,   s*   �	p	J:&c          C   s   d d  l  }  |  j j j �  S(   Ni����(   t   fipy.tests.doctestPlust   testst   doctestPlust   testmod(   t   fipy(    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyt   _test�  s    t   __main__N(   t   __docformat__t
   fipy.toolsR   R�   R   t   fipy.tools.decoratorsR   t   fipy.variables.cellVariableR   t   __all__R   R�   R�   (    (    (    sD   ./FiPy-3.0/fipy/models/levelSet/distanceFunction/distanceVariable.pyt   <module>#   s   	� � �	