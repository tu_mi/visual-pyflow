tools Package
=============

:mod:`~fipy.tools` Package
--------------------------

.. automodule:: fipy.tools
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.tools.copy_script` Module
-------------------------------------

.. automodule:: fipy.tools.copy_script
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.tools.debug` Module
-------------------------------

.. automodule:: fipy.tools.debug
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.tools.decorators` Module
------------------------------------

.. automodule:: fipy.tools.decorators
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.tools.dump` Module
------------------------------

.. automodule:: fipy.tools.dump
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.tools.inline` Module
--------------------------------

.. automodule:: fipy.tools.inline
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.tools.numerix` Module
---------------------------------

.. automodule:: fipy.tools.numerix
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.tools.parser` Module
--------------------------------

.. automodule:: fipy.tools.parser
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.tools.test` Module
------------------------------

.. automodule:: fipy.tools.test
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.tools.vector` Module
--------------------------------

.. automodule:: fipy.tools.vector
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.tools.vitals` Module
--------------------------------

.. automodule:: fipy.tools.vitals
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    fipy.tools.comms
    fipy.tools.dimensions
    fipy.tools.performance

