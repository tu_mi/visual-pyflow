matrices Package
================

:mod:`~fipy.matrices.offsetSparseMatrix` Module
-----------------------------------------------

.. automodule:: fipy.matrices.offsetSparseMatrix
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.matrices.pysparseMatrix` Module
-------------------------------------------

.. automodule:: fipy.matrices.pysparseMatrix
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.matrices.scipyMatrix` Module
----------------------------------------

.. automodule:: fipy.matrices.scipyMatrix
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.matrices.sparseMatrix` Module
-----------------------------------------

.. automodule:: fipy.matrices.sparseMatrix
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.matrices.test` Module
---------------------------------

.. automodule:: fipy.matrices.test
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.matrices.trilinosMatrix` Module
-------------------------------------------

.. automodule:: fipy.matrices.trilinosMatrix
    :members:
    :undoc-members:
    :show-inheritance:

