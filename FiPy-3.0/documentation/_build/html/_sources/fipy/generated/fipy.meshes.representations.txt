representations Package
=======================

:mod:`~fipy.meshes.representations.abstractRepresentation` Module
-----------------------------------------------------------------

.. automodule:: fipy.meshes.representations.abstractRepresentation
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.meshes.representations.gridRepresentation` Module
-------------------------------------------------------------

.. automodule:: fipy.meshes.representations.gridRepresentation
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`~fipy.meshes.representations.meshRepresentation` Module
-------------------------------------------------------------

.. automodule:: fipy.meshes.representations.meshRepresentation
    :members:
    :undoc-members:
    :show-inheritance:

