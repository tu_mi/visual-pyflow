#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, time, h5py
import numpy as np
sys.path.append('./FiPy-3.0')
import fipy as fp

def solve(input_file):
    """This method reads a input .h5 file, puts together the problem and appends
    the output h matrix, and the gauss gradient matrix."""
    #reads inputs###############################################################
    f = h5py.File(input_file, 'r+')
    #f = h5py.File(sys.argv[1], 'r+')
    input = f[u'input']
    Lx, Ly = input[u'x'][0,0], input[u'z'][0,0]#length and height of the domain
    nx, ny = input[u'nx'][0,0], input[u'nz'][0,0]#number of columns and rows
    K_input = input[u'k'][:,:]#hydraulic conductivity matrix, dim(n_rows,n_cols)
    h_left = input[u'h_left'][:,0]#fixed head on the left boundary
    h_right = input[u'h_right'][:,0]#fixed head on the right boundary
    h_top = input[u'h_top'][0,:]#fixed head on the top boundary
    h_bottom = input[u'h_bottom'][0,:]#fixed head on the bottom boundary
    q_left = input[u'q_left'][:,0]#fixed head gradient on the left boundary
    q_right = input[u'q_right'][:,0]#fixed head gradient on the right boundary
    q_top = input[u'q_top'][0,:]#fixed head gradient on the top boundary
    q_bottom = input[u'q_bottom'][0,:]#fixed head gradient on the bottom bndry
        
        
    #computations###############################################################
        
    dx, dy = float(Lx)/nx, float(Ly)/ny
    #we use the built-in mesh class in FiPy
    mesh=fp.Grid2D(dx=dx, dy=dy, nx=nx, ny=ny)
    #we define K for calculations as a Fipy cell variable
    K = fp.CellVariable(name = "hydraulic conductivity",
                              mesh = mesh,
                              value = K_input.flatten())
    #initiate the variable
    h = fp.CellVariable(name = "hydraulic head",
                              mesh = mesh,
                              value = 0.)   #for steady state we initialize on 0.
                                            #a transient problem would need real
                                            #physical initial conditions
        #set up boundary conditions, (left-out boundaries are 'no-flow' by default)
    X, Y = mesh.faceCenters
    for j in range(h_left.shape[0]):#left and right bnd
        facesLeft = (( mesh.facesLeft & (j*dy < Y) & (Y < (j+1)*dy) ))
        facesRight = (( mesh.facesRight & (j*dy < Y) & (Y < (j+1)*dy) ))
        if not np.isnan(h_left[j]): h.constrain(h_left[j], facesLeft)
        if not np.isnan(h_right[j]): h.constrain(h_right[j], facesRight)
        if not np.isnan(q_left[j]): h.faceGrad.constrain(-q_left[j], facesLeft)
        if not np.isnan(q_right[j]): h.faceGrad.constrain(q_right[j], facesRight)
    for i in range(h_top.shape[0]):#top and bottom bnd
        facesTop = (( mesh.facesTop & (i*dx < X) & (X < (i+1)*dx) ))
        facesBottom = (( mesh.facesBottom & (i*dx < X) & (X < (i+1)*dx) ))
        if not np.isnan(h_top[i]): h.constrain(h_top[i], facesBottom)
        if not np.isnan(h_bottom[i]): h.constrain(h_bottom[i], facesTop)
        #the non-correspondance between h_top&facesBottom is due to different
            #conventions in the gui (z=0 top left and z+^ downwards) and in fipy 
            #(z=0 bottom left and z+^ upwards). To avoid flipping the arrays, the
            #top cells in the gui are actually bottom cells in fipy and vice-versa
            #this is to be remembered when we set the free surface constrain, the 
            #actual Z for each cell will have to be evaluated as "domain_z - cell_z"
        if not np.isnan(q_top[i]): h.faceGrad.constrain(-q_top[i], facesBottom)
        if not np.isnan(q_bottom[i]): h.faceGrad.constrain(q_bottom[i], facesTop)
        #the sign convention here is q>0 means inflow (hence the '-' sign at the
            #top of the grid to respect the convention taken for the axis)

    #set up and solve the equation
    eq = fp.DiffusionTerm(coeff=K) == 0.
    eq.solve(var=h, solver=fp.GeneralSolver(iterations=20, tolerance=1e-5))

    #we save the result to the h5 file, under the date
    try: del(f['/output'])
    except: pass
    output = f.create_group('/output')
    output['h_ss']=np.reshape(h.value,(ny,nx))
    output['h_ss'].attrs['description']='hydraulic head at steady state, dim(nz,nx)'
    output['h_ss'].attrs['CLASS']='IMAGE'#these two lines make the default view
    output['h_ss'].attrs['IMAGE_VERSION']="1.2"#in hdfview to be an image
    output['ggh_ss']=np.reshape(h.grad.value,(2,ny,nx))
    output['ggh_ss'].attrs['description']=r'''Gauss gradient of the hydraulic head:
    it is expressed at the center of the cell (like the head), dim(2,nz,nx),
    it is the sum of the heads on the faces with the closest neighbors (arithmetic 
    mean) multiplied by the face area, the total being divided by the cell volume.
    It is derived from the divergence theorem for vectors h.ex and h.ey'''
    f.close()
