#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, time, h5py
import numpy as np
sys.path.append('./FiPy-3.0')
import fipy as fp
import matplotlib.pyplot as plt

#grid = fp.Grid2D(nx=3,ny=2)
#cv = fp.CellVariable(mesh = grid)
#fv = fp.FaceVariable(mesh = grid)
#vcv = fp.CellVariable(mesh=grid, value=np.arange(12).reshape(2,6), rank=1)
#print vcv
#vfv = fp.FaceVariable(mesh=grid, rank=1)
#print vfv
#print fp.PowerLawConvectionTerm(coeff = vfv)
#raw_input('...')


#this function performs a unit injection test
def solve_injection(input_file,x_inj,y_inj,D,t_list,name='test'):
    """This method reads a input .h5 file, puts together the problem and appends
    the output h matrix, and the gauss gradient matrix."""
    #reads inputs###############################################################
    f = h5py.File(input_file, 'r+')
    input = f[u'input']
    Lx, Ly = input[u'x'][0,0], input[u'z'][0,0]#length and height of the domain
    nx, ny = input[u'nx'][0,0], input[u'nz'][0,0]#number of columns and rows
    dx, dy = float(Lx)/nx, float(Ly)/ny
    K = input[u'k'][:,:]#hydraulic conductivity matrix, dim(n_rows,n_cols)
    phi = input[u'phi'][:,:]#porosity matrix, dim(n_rows,n_cols)
    h_left = input[u'h_left'][:,0]#fixed head on the left boundary
    h_right = input[u'h_right'][:,0]#fixed head on the right boundary
    h_top = input[u'h_top'][0,:]#fixed head on the top boundary
    h_bottom = input[u'h_bottom'][0,:]#fixed head on the bottom boundary
    q_left = input[u'q_left'][:,0]#fixed head gradient on the left boundary
    q_right = input[u'q_right'][:,0]#fixed head gradient on the right boundary
    q_top = input[u'q_top'][0,:]#fixed head gradient on the top boundary
    q_bottom = input[u'q_bottom'][0,:]#fixed head gradient on the bottom bndry
    
    output = f[u'output']
    h = output[u'h_ss'][:,:]
    gh = output[u'ggh_ss'][:,:,:]
    #h, phi, K = h.T, phi.T, K.T#use symmetry
    dhy=(h[1:,:]-h[:-1,:])
    dhx=(h[:,1:]-h[:,:-1])
    Ky=0.5*(K[1:,:]+K[:-1,:])
    Kx=0.5*(K[:,1:]+K[:,:-1])
    phiy=0.5*(phi[1:,:]+phi[:-1,:])
    phix=0.5*(phi[:,1:]+phi[:,:-1])
    vx,vy = -Kx/phix*dhx/dx, -Ky/phiy*dhy/dy
    v_top = np.where(np.isnan(q_top), 2*(h_top-h[0,:])*K[0,:]/dx/phi[0,:], q_top/phi[0,:])
    v_bottom = np.where(np.isnan(q_bottom), 2*(h[-1,:]-h_bottom)*K[-1,:]/dx/phi[-1,:], q_bottom/phi[-1,:])
    v_left = np.where(np.isnan(q_left), 2*(h_left-h[:,0])*K[:,0]/dx/phi[:,0], q_left/phi[:,0])
    v_right = np.where(np.isnan(q_right), 2*(h[:,-1]-h_right)*K[:,-1]/dx/phi[:,-1], q_right/phi[:,-1])
    vy = np.hstack([v_top.flatten(),vy.flatten(),v_bottom.flatten()])
    vx = np.hstack([v_left.flatten(),vx.flatten(),v_right.flatten()])
    v  = np.vstack([ np.hstack([vx,np.zeros(vy.shape)]) , np.hstack([np.zeros(vx.shape),vy]) ])
    #plt.quiver(-K*gh[0,:,:],-K*gh[1,:,:])
    v2=(-K*gh/phi).reshape(2,nx*ny)
    #plt.quiver(vy.reshape(21,30),0.)
    #plt.quiver(0.,vx.reshape(20,31))
    #plt.show()
    f.close()

    #computations###############################################################
    #we use the built-in mesh class in FiPy
    mesh=fp.Grid2D(dx=dx, dy=dy, nx=nx, ny=ny)
    #we define v for calculations as a Fipy cell variable
    #v = fp.FaceVariable(name = "hydraulic conductivity",
    #                          mesh = mesh, value = v)
    v = fp.CellVariable(name = "pore fluid velocity (m/s)",
                              mesh = mesh, value = v2, rank=1)
    #initiate the variable, concentration
    c = fp.CellVariable(name = "normalized concentration",
                              mesh = mesh,
                              value = 0.)
    ind=np.floor(x_inj/Lx*nx)+nx*np.floor(y_inj/Ly*ny)
    c.value[ind]=1.#initial condition here
                              
    #set up boundary conditions, (left-out boundaries are 'no-flow' by default)
    X, Y = mesh.faceCenters
    for j in range(h_left.shape[0]):#left and right bnd
        facesLeft = (( mesh.facesLeft & (j*dy < Y) & (Y < (j+1)*dy) ))
        facesRight = (( mesh.facesRight & (j*dy < Y) & (Y < (j+1)*dy) ))
        v.constrain(-v_left[j], facesLeft)
        v.constrain(v_right[j], facesRight)
        c.constrain(0., facesLeft)
        c.constrain(0., facesRight)
    for i in range(h_top.shape[0]):#top and bottom bnd
        facesTop = (( mesh.facesTop & (i*dx < X) & (X < (i+1)*dx) ))
        facesBottom = (( mesh.facesBottom & (i*dx < X) & (X < (i+1)*dx) ))
        c.constrain(0, facesBottom)
        c.constrain(0, facesTop)
        #the non-correspondance between h_top&facesBottom is due to different
            #conventions in the gui (z=0 top left and z+^ downwards) and in fipy 
            #(z=0 bottom left and z+^ upwards). To avoid flipping the arrays, the
            #top cells in the gui are actually bottom cells in fipy and vice-versa
            #this is to be remembered when we set the free surface constrain, the 
            #actual Z for each cell will have to be evaluated as "domain_z - cell_z"
        v.constrain(-v_top[i], facesBottom)
        v.constrain(v_bottom[i], facesTop)
        #the sign convention here is q>0 means inflow (hence the '-' sign at the
            #top of the grid to respect the convention taken for the axis)

    #set up and solve the equation
    eq = fp.TransientTerm(coeff=1.) + fp.PowerLawConvectionTerm(coeff=v) == fp.DiffusionTerm(coeff=D)
    timesteps=[t_list[0]]+[t_list[i]-t_list[i-1] for i in range(1,len(t_list))]
    for t in range(len(timesteps)):
        timestep = timesteps[t]
        #first solving the equation
        eq.solve(var=c, dt=timestep, solver=fp.GeneralSolver(iterations=20, tolerance=1e-5))
        #now plotting the results
        plt.close()#for safety
        plt.pcolor(np.arange(nx+1)/float(nx)*Lx,np.arange(ny+1)/float(ny)*Ly,
                   c.value.reshape(ny,nx)[-1::-1,:],edgecolor='k')
        plt.title('normalized concentration')
        plt.axis('equal')
        plt.colorbar()
        plt.quiver(np.linspace(Lx/nx/2.,Lx-Lx/nx/2.,nx,endpoint=True),
                   np.linspace(Ly/ny/2.,Ly-Ly/ny/2.,ny,endpoint=True),
                   v2[0,:].reshape(ny,nx)[-1::-1,:],-v2[1,:].reshape(ny,nx)[-1::-1,:])
        plt.savefig(name+'_t='+str(t_list[t])+'_s.png')
        plt.close()
#solve_injection("My_test_aquifer.h5",0.2,0.3,0.,[1e3,2e3,3e3,4e3],'test2')
