#!/usr/bin/python
# -*- coding: utf-8 -*-

WORK_DIRECTORY="/home/benoitsu/visual-pyflow"

# std libraries
import sys , os
import functools

# gui libraries
from PySide import QtGui, QtCore

# other third party libs
import numpy as np
import h5py
import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4'] = 'PySide'
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

# our own scripts
from pollock import *
import saturated_gwflow_solver as satflow
import injection_test as inject

DEFAULT_K = 1.5e-5
DEFAULT_PHI = 20
DEFAULT_H = "NaN"
DEFAULT_Q = 10
CMAP = matplotlib.cm.jet

###########
############ Run class, saves the data and runs the script with the solver
class Run(QtGui.QWidget):
    default = True
    def __init__(self, parent=None):
        super(Run, self).__init__()
        
        filename = project_name + ".h5"
        main_win.save(filename)
        satflow.solve(filename)
        
        main_win.visualization_bt.setEnabled(True)  # Ben^#            
#######
####### Defines boundary conditions 
class Boundaries(QtGui.QWidget):
    default = True
    
    def __init__(self , parent=None):
        super(Boundaries, self).__init__()
        
        
        #clears pollock lines and isolines
	try:
		for line in pollock_list:
		    main_win.view.scene.removeItem(line)
		for line in isolines_list:
		    main_win.view.scene.removeItem(line)
        except RuntimeError:
            pass

        try:        
            main_win.view.scene.removeItem(main_win.view.injection_point) 
        except AttributeError:
            pass      

        # creates global dictionaries to store boundary conditions 
        global bcond_dict  # Dictionary of boundaries {key : ( right , 5 ) : [ 0 : group , 1 : line , 2 : ghost cell , 3 : arrow in , 4 : arrow out , 5 : h , 6 : q] }     
        if Boundaries.default: 
            bcond_dict = {}  # initializes new empty dictionary only if no boundary items are on the scene   
        
        ####  Decides over what should be selectable and how return should behave
        main_win.view.scene.mode = "boundaries"  # mode = "boundaries" / "cells"  controls th behavior of View.keyPressEvent()
        # cell objects set not selectable
        for item in cell_dict:
            item.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, False)
        # boundary objects set selectable
        for item in bcond_dict:
            bcond_dict[item][0].setFlag(QtGui.QGraphicsItem.ItemIsSelectable, True)
        # if empty boundary condition dictionary, create and display items
        if not bcond_dict:  
            main_win.view.create_edges()
            main_win.view.color_boundaries()
            
            
        
        # waring(self, "", "please enter floats!" ) 
        # calls initUI to design widget    
        self.initUI()  
        
    def initUI(self):      
        # create second row : 2 line edits
        self.hbox_le = QtGui.QHBoxLayout()
        self.le_h = QtGui.QLineEdit()  # Creates line edits for h and q     
        self.le_q = QtGui.QLineEdit()
        #add values here for the le's'
        self.hbox_le.addWidget(self.le_h)
        self.hbox_le.addWidget(self.le_q)
        
        # Creates first row : two Radiobuttons with labels
        self.vbox = QtGui.QVBoxLayout(self) 
        self.hbox_header = QtGui.QHBoxLayout()
        self.radio_head = QtGui.QRadioButton("Hydaulic Head (m)")  
        self.radio_head.toggled.connect(self.toggle_h)
        self.hbox_header.addWidget(self.radio_head)
        self.radio_flow = QtGui.QRadioButton("Flow (m/s)")
        self.radio_flow.toggled.connect(self.toggle_q)
        self.hbox_header.addWidget(self.radio_flow)
        
        # Creates a space to isolate ok/cancel button
        self.strunt = QtGui.QLabel("  ")
        self.vspace = QtGui.QHBoxLayout()
        self.vspace.addWidget(self.strunt)   
 
        # Creates OK and cancel buttons
        self.hbox_bottom = QtGui.QHBoxLayout()
        self.hbox_bottom.addStretch()
        self.ok_button = QtGui.QPushButton("OK")
        
        self.ok_button.clicked.connect(self.getValues)
        self.ok_button.setDisabled(True)
        self.hbox_bottom.addWidget(self.ok_button)
        self.hbox_bottom.addStretch()
    
        self.cancel_button = QtGui.QPushButton("Cancel")
        self.cancel_button.clicked.connect(self.close)
        self.hbox_bottom.addWidget(self.cancel_button)
        self.hbox_bottom.addStretch()
        
        # places horizontal layouts in the vertical one
        self.vbox.addLayout(self.hbox_header)
        self.vbox.addLayout(self.hbox_le)
        self.vbox.addLayout(self.vspace)
        self.vbox.addLayout(self.hbox_bottom)
        
        # create / update legend colorbar
        a = [float(i[5]) for i in bcond_dict.itervalues()]
        main_win.legend.addItem(Legend_bar_item(datasets = [a] , title='head (m)'))
        main_win.update_legend()
        
    # Disable h , set it to NaN , and let you choose q
    def toggle_q(self):
        self.le_h.setDisabled(True)
        self.le_h.setText("NaN")
        self.le_q.setEnabled(True)
        self.le_q.clear()
        self.ok_button.setEnabled(True)
    
    # Disable q , set it to NaN , and let you choose h    
    def toggle_h(self):
        self.le_q.setDisabled(True)
        self.le_q.setText("NaN")
        self.le_h.setEnabled(True)
        self.le_h.clear()
        self.ok_button.setEnabled(True)

    # reads entries and changes values in bcond_dict
    def getValues(self):   
        try :  # try , avoids non-floats to be read
            # values from line-edits
            h = np.float(self.le_h.text()) 
            q = np.float(self.le_q.text())

            for key  in bcond_dict.keys():  # goes through all bcond dictionary entries
                for item in main_win.view.selectedItems:  # goes through all selected items
                    if bcond_dict[key][0] == item:
                        bcond_dict[key][5] = h  
                        bcond_dict[key][6] = q
            
            # call function to redraw boundaries objects
            main_win.view.color_boundaries()

            # update the legend
            main_win.legend = Legend()
#             h_bnd = np.array(bcond_dict.values())[:, 5].astype(float)

            a = [float(i[5]) for i in bcond_dict.itervalues()]
            main_win.legend.addItem(Legend_bar_item(datasets = [a] , title='head (m)'))
            main_win.update_legend()
            
            # close widget
            self.close()
        except ValueError:
            QtGui.QMessageBox.warning(self, "", "please enter floats!") 
#########################################################################################


###########
########### Widget for value and color selection of cells
class Props(QtGui.QWidget):
    default = True
    bt_dict = {}
    
    def __init__(self, parent=None):
        super(Props, self).__init__()

        # clears selection
        main_win.view.scene.clearSelection()
        #clears pollock lines and isolines
        try:
            for line in pollock_list:
                main_win.view.scene.removeItem(line)
        except RuntimeError:
            pass
        
        try:        
            main_win.view.scene.removeItem(main_win.view.injection_point) 
        except AttributeError:
            pass      
    
        try:
            for line in isolines_list:
                main_win.view.scene.removeItem(line)
        except RuntimeError:
            pass
        
        # mode = "boundaries" / "cells"  controls th behavior of View.keyPressEvent()
        main_win.view.scene.mode = "cells"   
        
        # makes cell objects selectable
        for item in cell_dict:
            item.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, True) 
        # makes boundary-objects non-selectable if they exist
        try:
            if bcond_dict :  # necessary ??
                for item in bcond_dict:
                    bcond_dict[item][0].setFlag(QtGui.QGraphicsItem.ItemIsSelectable, False)
        except NameError:
            pass
        
        # initiates a list of selected items 
        self.selected = None    
        
        # draw widget
        self.initUI()  
                    
    def initUI(self):      
        
        self.vbox = QtGui.QVBoxLayout(self) 

        self.hbox_header = QtGui.QHBoxLayout()
        self.hbox_header.addWidget(QtGui.QLabel(" "))
        
        self.label_cond = QtGui.QLabel("  Hydaulic  \n Conductivity \n   (m/s)")  
#         self.label_cond.setMinimumWidth(50)
        self.hbox_header.addWidget(self.label_cond)
             
        self.label_poro = QtGui.QLabel("Porosity \n (%) ")
#         self.label_cond.setMinimumWidth(50)
        self.hbox_header.addWidget(self.label_poro)
        self.vbox.addLayout(self.hbox_header)
        
        # create the row with default values
        self.hbox_default = QtGui.QHBoxLayout()
        self.default_bt = QtGui.QPushButton("default")
        
        # Creates line edits for k and phi  
        self.default_cond = QtGui.QLineEdit()
        self.default_poro = QtGui.QLineEdit()
        self.hbox_default.addWidget(self.default_bt)

        self.hbox_default.addWidget(self.default_cond)
        self.hbox_default.addWidget(self.default_poro)
        
        # create plus button to fetch default material value
        self.plus = QtGui.QToolButton()
        img = os.path.join(WORK_DIRECTORY,"images","plus.png")
#        img = resource_path("images/plus.png")
#         img = os.path.relpath("images/plus.png", "./")
        self.plus.setMaximumHeight(25)
        self.plus.setMaximumWidth(25)
        self.plus.setIcon(QtGui.QIcon(img))
        self.plus.setCheckable(True)
#         self.plus.setVisible(False)
        self.plus.clicked.connect(functools.partial(self.list_of , "button0"))
#         self.hbox_default.addWidget(self.plus)
        self.hbox_default.addStretch()
    
        if Props.default:
        # bt_dict[button] = [ k , phi , col  , le_cond , le_poro , button , name ] 
            global bt_dict
            bt_dict = {}
            # bt_dict[button] =      [     k       ,    phi      ,      col                  , le_cond            ,  le_poro , button ] 
            bt_dict["button0"] = [  DEFAULT_K  , DEFAULT_PHI  , QtGui.QColor("#ffffff")  , self.default_cond , self.default_poro  , self.default_bt , "Default" ]
  
        def_style = "QPushButton { border: 2px solid #8f8f91; border-radius: 6px; background-color: %s; min-width: 80px;}" % bt_dict["button0"][2].name()
        self.default_bt.setStyleSheet(def_style) 
            
        self.default_cond.setText(str(bt_dict["button0"][0]))
        self.default_cond.setMaximumWidth(80)
        self.default_poro.setText(str(bt_dict["button0"][1]))
        self.default_poro.setMaximumWidth(80)

        self.default_cond.textChanged.connect(functools.partial(self.change_values , "button0", 0))
        self.default_poro.textChanged.connect(functools.partial(self.change_values , "button0", 1))
        self.default_bt.clicked.connect(functools.partial(self.choose_color, "button0", self.default_bt))  # Uses module FUNCTOOLS to pass argument with a slot
        
        bt_dict["button0"][3:6] = [ self.default_cond , self.default_poro , self.default_bt, "Default" ]
        
        self.vbox.addLayout(self.hbox_default)
                
        ###    Creates colored buttuns for the different values
        NB_BT = 6
        for index in range(0, NB_BT):
            hbox = QtGui.QHBoxLayout()  # horizontal layout
            
            self.bt = QtGui.QPushButton("a")  # creates a button 
            self.bt_str = "button%s" % (index + 1)
            self.pb_firstRelease = QtCore.QTime(0, 0, 0, 0)  # memorizes time of clicking for determining doubleclicks
            self.pb_secondRelease = QtCore.QTime(0, 0, 0, 0)

            # Initiates color 
            inc = 255 / NB_BT  #  flexible increament to  let the nb of values vary
            if Props.default:
                col = QtGui.QColor(inc * index, 0, 255 - inc * index)  # creates colors from blue to red
            else:
                col = bt_dict[self.bt_str][2]
                
            # # defines a style to modify the pushbuttons 
            style = """QPushButton { border: 2px solid #8f8f91; border-radius: 6px; background-color: %s; min-width: 80px;}""" % col.name() 
            self.bt.setStyleSheet(style) 
            
            # creates line edits for k and phi
            le_cond = QtGui.QLineEdit()
            le_cond.setMaximumWidth(80)
            le_poro = QtGui.QLineEdit()
            le_poro.setMaximumWidth(80)

            # Creates initial K and phi values
            if Props.default:
                # Stores data coupled to button
                # bt_dict[button] =  [    k  ,  phi   ,  col , le_cond ,  le_poro , button ] 
                bt_dict[self.bt_str] = [  0  , 0  , 0  , 0 , 0  , 0 , 0 ]  # Dictionary { button : [ k, phi , color , con line edit, poro line edit, bt]} 
                bt_dict[self.bt_str][0] = 10 ** -(index + 2) 
                bt_dict[self.bt_str][1] = 20
                bt_dict[self.bt_str][6] = "value %s" % (index + 1)
                if index == NB_BT - 1:
                    Props.default = False

            bt_dict[self.bt_str][2:6] = [col , le_cond , le_poro , self.bt, bt_dict[self.bt_str][6] ]
            
            # gives the button a name
            self.bt.setText(bt_dict[self.bt_str][6])    
            
            # Connects buttons with "choose_color" method
            self.bt.clicked.connect(functools.partial(self.choose_color, self.bt_str, self.bt))  # Uses module FUNCTOOLS to pass argument with a slot
            
            # provides the line edit with values
            le_cond.setText(str(bt_dict[self.bt_str][0]))
            le_cond.textChanged.connect(functools.partial(self.change_values , self.bt_str, 0))
            le_poro.setText(str(bt_dict[self.bt_str][1]))
            le_poro.textChanged.connect(functools.partial(self.change_values , self.bt_str, 1))

            # create plus button to fetch default material value
            self.plus = QtGui.QToolButton()
            img = os.path.join(WORK_DIRECTORY,"images","plus.png")
#            img = resource_path("images/plus.png")  
#             img = os.path.relpath("images/plus.png", "./")
            self.plus.setMaximumHeight(25)
            self.plus.setMaximumWidth(25)
            self.plus.setIcon(QtGui.QIcon(img))
            self.plus.setCheckable(True)
            self.plus.clicked.connect(functools.partial(self.list_of , self.bt_str))
#             self.hbox_default.addWidget(self.plus)

            # put things in the layout
            hbox.addWidget(self.bt)
            hbox.addWidget(le_cond)
            hbox.addWidget(le_poro)
            hbox.addWidget(self.plus)
            self.vbox.addLayout(hbox)
        
        # Creates a space to isolate ok/cancel button
        self.strunt = QtGui.QLabel("  ")
        self.vspace = QtGui.QHBoxLayout()
        self.vspace.addWidget(self.strunt)   
        self.vbox.addLayout(self.vspace)

        # Creates OK and cancel buttons
        self.hbox_bottom = QtGui.QHBoxLayout()
        self.hbox_bottom.addStretch()
        self.ok_button = QtGui.QPushButton("Assign")
        self.ok_button.setDisabled(True)
#         self.ok_button.clicked.connect(self.getValues)
        self.ok_button.clicked.connect(main_win.view.assign)
        self.hbox_bottom.addWidget(self.ok_button)
#         self.hbox_bottom.addStretch()
        self.cancel_button = QtGui.QPushButton("Close")
        self.cancel_button.clicked.connect(self.close)
        self.hbox_bottom.addWidget(self.cancel_button)
# create plus button to fetch default material value
        self.info = QtGui.QToolButton()
        img = os.path.join(WORK_DIRECTORY,"images","info.png")
#        img = resource_path("images/info.png")
#         img = os.path.relpath("images/info.png", "./")
        self.info.setMaximumHeight(25)
        self.info.setMaximumWidth(25)
        self.info.setIcon(QtGui.QIcon(img))
        self.info.setCheckable(True)
        self.info.clicked.connect(self.show_info)
        self.hbox_bottom.addWidget(self.info)
        self.hbox_bottom.addStretch()

        self.vbox.addLayout(self.hbox_bottom)

        main_win.view.recolor()
        # Define window/widget appearance
        self.setGeometry(QtCore.QRect(1050, 150, 307, 150))

        self.setWindowTitle("Properties")
            
        # set an icon in the corner
        img = os.path.join(WORK_DIRECTORY,"images","water.png")
#        img = resource_path("images/water.png")
#         img = os.path.relpath("images/water.png", "./")
        self.setWindowIcon(QtGui.QIcon(img))

            
    def show_info(self):
        #Pops up inormation about how to edit material properties
        QtGui.QMessageBox.information(self, "How to set maerial conditions.",
                """ To edit internal properties:
                - Edit values to parameterize the different materials.
                - Press ctrl and click on a colored button to edit its color.
                - You can load example materials by clicking the plus signs.
                - Select a material by clicking one of the colored buttons.
                - Select cells in grid in the main view.
                - Press "Assign" or hit the return key to edit chosen cells
                - Voila !!
                """)
             
    def list_of(self, bt_str):
        self.list = List_widget(bt_str)
        self.list.show()
    
    ############   Let you choose a set of values , get button as argument, activate color dialog on double-click
    def choose_color(self, button_str, button):
        self.selected = button_str  # value selected, button replaces None
    
        global bt_dict, selected_bt  # , k_selected, phi_selected , col_selected
        selected_bt = self.selected
        
        self.ok_button.setDisabled(False)
        # Handles button appearance, set all other buttons to unselected appearance 

        for entry in bt_dict:   
            col = bt_dict[entry][2]  # color set to buttons initial color
            style = "QPushButton { border: 2px solid #8f8f91; border-radius: 6px; background-color: %s; min-width: 80px;}" % col.name()
            bt_dict[entry][5].setStyleSheet(style)
            
        col = bt_dict[button_str][2]  # color set to buttons initial Color
        style = """QPushButton { border: 4px solid #000000; border-radius: 6px; background-color: %s; min-width: 80px;}""" % col.name()
        bt_dict[button_str][5].setStyleSheet(style)  # Thick black border style added
        

        # Identifies ctrl+click if not default button 
        if button != self.default_bt:
            modifiers = QtGui.QApplication.keyboardModifiers()
            if modifiers == QtCore.Qt.ControlModifier:
                
                col = QtGui.QColorDialog.getColor()
                if col.name() != "#000000" and col.name() != "#ffffff":
                    bt_dict[button_str][2] = col
                    style = """QPushButton { border: 4px solid #000000; border-radius: 6px; background-color: %s; min-width: 80px;}""" % col.name()
                    main_win.view.recolor()
                    
                    button.setStyleSheet(style)
                if col.name() == "#ffffff" : 
                    QtGui.QMessageBox.warning(self, "", "White is reserved to default values!")

        main_win.view.recolor()

    def change_values(self , button, index, entry):
        # entry and new_k are equivalent maybe we could remove  line edits from bt_dict
        try:
            new_k = entry  # bt_dict[button][3].text()
            bt_dict[button][index] = float(new_k)

        except ValueError:
            QtGui.QMessageBox.warning(self, "", "please enter floats!") 
        
    # Test if values are correct and reads them         
    def getValues(self):    
        try :
            if not self.selected:
                QtGui.QMessageBox.warning(self, "", "You must select a set of values before applying it to cells!")
            else:
                global bt_dict, selected_bt  # , k_selected, phi_selected , col_selected
                selected_bt = self.selected
                
                self.close()
        
            main_win.legend = Legend()
            main_win.update_legend()
        
        except ValueError:
            QtGui.QMessageBox.warning(self, "", "please enter floats!") 
############################################################################################################


###########
########### Displays a list of suggested material values
class List_widget(QtGui.QListWidget):
 
    def __init__(self, bt_str, parent=None):
        super(List_widget, self).__init__()    
        
        self.bt_str = bt_str
        
        self.sediments = [ "gravel" , "coarse sand", "sand"  , "silt"    , "clay"    ,  "igneous 1" , "igneous 2"   ]
        self.conductivity = [    0.1   , 0.01    , 0.03   , 0.00000001  , 0.0000000001 , 0.00000001     ,   0.000000001 ]
        self.porosity = [        35    , 30     ,   30    ,      45      ,    50      ,     3        ,          1         ]
        self.color = [ "#98aa12" , "#ff65eb"   , "#123e46", "#feda01" , "#aeea65", '#abab11', '#baba22']
        
        self.listwidget = QtGui.QListWidget()
        for i in range(len(self.sediments)):
            item = QtGui.QListWidgetItem(self.listwidget)
            item.setText(" %s :   %s , %s " % (self.sediments[i], self.conductivity[i], self.porosity[i]))
            item.setBackground(QtGui.QColor(self.color[i]))
        
        self.ok_bt = QtGui.QPushButton("OK")
        self.ok_bt.clicked.connect(self.assign)
        self.cancel_bt = QtGui.QPushButton("Cancel")
        self.cancel_bt.clicked.connect(self.close)
        
        # set layouts
        self.vbox = QtGui.QVBoxLayout()
        self.hbox1 = QtGui.QHBoxLayout()
        self.hbox1.addWidget(self.listwidget)
        self.hbox2 = QtGui.QHBoxLayout()
        self.hbox2.addWidget(self.ok_bt)
        self.hbox2.addWidget(self.cancel_bt)
                
        self.vbox.addLayout(self.hbox1)
        self.vbox.addLayout(self.hbox2)
        self.setLayout(self.vbox)
                
        # set an icon in the corner
        img = os.path.join(WORK_DIRECTORY,"images","water.png")
#        img = resource_path("images/water.png")
#         img = os.path.relpath("images/water.png", "./")
        self.setWindowIcon(QtGui.QIcon(img))

        

    def assign(self):
        indexes = self.listwidget.selectedIndexes()
        index = indexes[0].row()
        bt_dict[self.bt_str][0] = self.conductivity[index]
        bt_dict[self.bt_str][1] = self.porosity[index]
        bt_dict[self.bt_str][2] = QtGui.QColor(self.color[index])
        bt_dict[self.bt_str][6] = self.sediments[index]
        geo = main_win.props.geometry()
        main_win.props.close()
        main_win.props = Props()
        main_win.props.setGeometry(geo)
        main_win.props.show()
        main_win.props
        self.close()
###########################################################################################################

###########
############ Mesh class, widget to define # elements in domain
class Mesh(QtGui.QWidget):
    default = True
    
    def __init__(self, parent=None):
        super(Mesh, self).__init__()    
        self.initUI()
        
    def initUI(self):      
    
        # create layouts
        self.hbox1 = QtGui.QHBoxLayout()
        self.hbox2 = QtGui.QHBoxLayout()
        self.hbox3 = QtGui.QHBoxLayout()
        self.hbox4 = QtGui.QHBoxLayout()
        
        # create layouts
        self.vbox = QtGui.QVBoxLayout()
        self.vbox.addLayout(self.hbox1)
        self.vbox.addLayout(self.hbox2)
        self.vbox.addLayout(self.hbox3)
        self.vbox.addLayout(self.hbox4)

        # create buttons and add them to the layout boxes
        self.btn_x = QtGui.QPushButton("number of columns :", self)
        self.btn_x.setMinimumWidth(120)
        self.hbox1.addWidget(self.btn_x)
        self.btn_x.clicked.connect(self.showDialog_x)
        self.btn_z = QtGui.QPushButton("number of rows :", self)
        self.btn_z.setMinimumWidth(120)
        self.hbox2.addWidget(self.btn_z)
        self.btn_z.clicked.connect(self.showDialog_z)

        self.btn_ok = QtGui.QPushButton("OK", self)
        self.hbox4.addStretch()
        self.hbox4.addWidget(self.btn_ok)
        self.hbox4.addStretch()
        
        # creates line edits to read and write values
        self.le_x = QtGui.QLineEdit(self)
        if Mesh.default:
            self.le_x.setText("30")
        else:
            self.le_x.setText(str(nb_x))
        self.hbox1.addWidget(self.le_x)
        self.le_z = QtGui.QLineEdit(self)
        if Mesh.default:
            self.le_z.setText("20")
        else:
            self.le_z.setText(str(nb_z))
        self.hbox2.addWidget(self.le_z)

        self.setLayout(self.vbox)
        
        # Define window/widget appearance
#         self.setGeometry(QtCore.QRect(0, 0, 640, 480))
        self.setWindowTitle("Enter number of discrete elements")
        
        # get values and close if OK pressed
        self.btn_ok.clicked.connect(self.getValues)
        
        
        # set an icon in the corner
        img = os.path.join(WORK_DIRECTORY,"images","water.png")
#        img = resource_path("images/water.png")
#         img = os.path.relpath("images/water.png", "./")
        self.setWindowIcon(QtGui.QIcon(img))

        
    # writes current values in the    
    # get values and close if enter-key pressed
    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Return :
            self.getValues()
    
    # implement dialog boxes (a bit useless though)
    def showDialog_x(self):
        text, ok = QtGui.QInputDialog.getText(self, 'Input Dialog', 'Enter value:')
        if ok:
            self.le_x.setText(str(text))
    # implement dialog boxes (a bit useless though)           
    def showDialog_z(self):
        text, ok = QtGui.QInputDialog.getText(self, 'Input Dialog', 'Enter value:')
        if ok:
            self.le_z.setText(str(text))

    # Test if values are correct and reads them         
    def getValues(self):    
        try :
            self.xvalue = self.le_x.text()
            self.nb_x = int(self.xvalue)
            self.zvalue = self.le_z.text()
            self.nb_z = int(self.zvalue)
             
            global nb_x, nb_z  # , k_matrix, value_matrix
            nb_x = self.nb_x
            nb_z = self.nb_z 
            
            for item in main_win.view.frame.childItems():
                main_win.view.scene.removeItem(item)
                
            main_win.view.discretize_domain(self.nb_x, self.nb_z)

            self.le_x.setText(str(self.nb_x))
            self.le_z.setText(str(self.nb_z))
            
            # initializes bt:_dict to  default button
            global bt_dict
            bt_dict = {}
            # bt_dict[button] =      [     k       ,    phi      ,      col                  , le_cond            ,  le_poro , button ] 
            bt_dict["button0"] = [  DEFAULT_K  , DEFAULT_PHI  , QtGui.QColor("#ffffff")  , 0 , 0  , 0 , "Default"]
  
            
            # set default to classes
            Mesh.default = False
            Props.default = True
            Boundaries.default = True
            
            # Enables appropriate buttons
            main_win.props_bt.setEnabled(True)
            main_win.boundaries_bt.setEnabled(True)
            main_win.run_bt.setEnabled(False)
            main_win.visualization_bt.setEnabled(False)
            main_win.pollock.setChecked(False)
            main_win.pollock.setEnabled(False)
#             main_win.square.setChecked(True)

            #empties dictionary of BC
            global bcond_dict
            bcond_dict = {}
            
            Mesh.default = False
            
            main_win.legend = Legend()
            main_win.update_legend()
            
            self.close()
            
            main_win.legend = Legend()
            main_win.update_legend()

            
        except ValueError:
            QtGui.QMessageBox.warning(self, "", "please enter an integer!") 
############################################################################################################


###########
########### Start class, opens a widget that reads domain dimensions
class Start(QtGui.QWidget):
    default = True
    
    def __init__(self, parent=None):
        super(Start, self).__init__()
        self.initUI()
        
    def initUI(self):      
        
        # create layouts
        self.hbox1 = QtGui.QHBoxLayout()
        self.hbox2 = QtGui.QHBoxLayout()
        self.hbox3 = QtGui.QHBoxLayout()
        self.hbox4 = QtGui.QHBoxLayout()
        
        # create layouts
        self.vbox = QtGui.QVBoxLayout()
        self.vbox.addLayout(self.hbox1)
        self.vbox.addLayout(self.hbox2)
        self.vbox.addLayout(self.hbox3)
        self.vbox.addLayout(self.hbox4)

        # create buttons and add them to the layout boxes
        self.btn_x = QtGui.QPushButton("Domain's length (m) :", self)
        self.hbox1.addWidget(self.btn_x)
        self.btn_x.clicked.connect(self.showDialog_x)
        self.btn_z = QtGui.QPushButton("Domain's depth (m) :", self)
        self.hbox2.addWidget(self.btn_z)
        self.btn_z.clicked.connect(self.showDialog_z)
        self.btn_exag = QtGui.QPushButton("Vertical exageration :", self)
        self.hbox3.addWidget(self.btn_exag)
        self.btn_exag.clicked.connect(self.showDialog_exag)
        self.btn_exag.setDisabled(True)
        self.btn_ok = QtGui.QPushButton("OK", self)
        self.hbox4.addStretch()
        self.hbox4.addWidget(self.btn_ok)
        self.hbox4.addStretch()
        
        # creates line edits to read and write values
        self.le_x = QtGui.QLineEdit(self)
        if Start.default:
            self.le_x.setText("500.0")
        else:
            self.le_x.setText(str(domain_x))
        self.hbox1.addWidget(self.le_x)
        
        self.le_z = QtGui.QLineEdit(self)
        if Start.default:
            self.le_z.setText("200.0")
        else:
            self.le_z.setText(str(domain_z))
            
        self.hbox2.addWidget(self.le_z)
        self.le_exag = QtGui.QLineEdit(self)
        self.le_exag.setDisabled(True)
        self.hbox3.addWidget(self.le_exag)
        self.setLayout(self.vbox)
        
        # Define window/widget appearance
        self.setWindowTitle("Define domain's dimension")
        
        # get values and close if OK pressed
        self.btn_ok.clicked.connect(self.getValues)
        
        # set an icon in the corner
        img = os.path.join(WORK_DIRECTORY,"images","water.png")
#        img = resource_path("images/water.png")
#         img = os.path.relpath("images/water.png", "./")
        self.setWindowIcon(QtGui.QIcon(img))

        
    # get values and close if enter-key pressed
    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Return :
            self.getValues()
    
    # implement dialog boxes (a bit useless though)
    def showDialog_x(self):
        text, ok = QtGui.QInputDialog.getText(self, 'Input Dialog', 'Enter value:')
        if ok:
            self.le_x.setText(str(text))
    # implement dialog boxes (a bit useless though)           
    def showDialog_z(self):
        text, ok = QtGui.QInputDialog.getText(self, 'Input Dialog', 'Enter value:')
        if ok:
            self.le_z.setText(str(text))
    # implement dialog boxes (a bit useless though)        
    def showDialog_exag(self):
        text, ok = QtGui.QInputDialog.getText(self, 'Just Kiddind ;-p', 'Enter value if you want...')
        if ok:
            self.le_exag.setText(str(text))
    
    # Test if values are correct and reads them         
    def getValues(self):    
        try :
            self.xvalue = self.le_x.text()
            self.domain_x = float(self.xvalue)
            self.zvalue = self.le_z.text()
            self.domain_z = float(self.zvalue)
            self.exagvalue = self.le_exag.text()
             
            global domain_x, domain_z 
            domain_x = self.domain_x
            domain_z = self.domain_z
            
#             for item in main_win.view.scene.items():
            try: main_win.view.scene.removeItem(main_win.view.frame)
            except: pass
            
            main_win.view.create_domain(self.domain_x, self.domain_z)
            
#             main_win.set_scene(main_win.view)
            main_win.mesh_bt.setEnabled(True)
            main_win.props_bt.setEnabled(False)
            main_win.boundaries_bt.setEnabled(False)
            main_win.run_bt.setEnabled(False)
            main_win.visualization_bt.setEnabled(False)
            main_win.pollock.setChecked(False)
            main_win.pollock.setEnabled(False)

            # set default to classes
            Start.default = False 
            Mesh.default = True
            Props.default = True
            Boundaries.default = True
            self.close()

            main_win.legend = Legend()
            main_win.update_legend()
            
        # Displays Warning pop-up window if values not float
        except ValueError:
            QtGui.QMessageBox.warning(self, "", "     please enter a float! \n \n Decimal separator should be '.' ")
############################################################################################################

###########
########### Set project name (and mode choice)
class New(QtGui.QWidget):
    default = True
    
    def __init__(self):
        super(New, self).__init__()
        main_win.setEnabled(False)
        
        # disable buttons
        main_win.mesh_bt.setEnabled(False)
        main_win.props_bt.setEnabled(False)
        main_win.boundaries_bt.setEnabled(False)
        main_win.run_bt.setEnabled(False)
        
        # set classes to default as if they were never used
        Props.default = True
        self.initUI()
        
        
        
    def initUI(self):      
        self.label0 = QtGui.QLabel('New project') 
        self.hbox0 = QtGui.QHBoxLayout()
        self.hbox0.addWidget(self.label0)
        
        self.label1 = QtGui.QLabel('Creates a new Future Learn project. \n')
        self.hbox1 = QtGui.QHBoxLayout()
        self.hbox1.addWidget(self.label1)
        
        self.label2 = QtGui.QLabel("    ")
        self.hbox2 = QtGui.QHBoxLayout()
        self.hbox2.addWidget(self.label2)
        
        self.label3 = QtGui.QLabel("Enter project name :")
        self.le = QtGui.QLineEdit()
        if self.default:
            self.le.setText("My_first_aquifer")
        else:
            self.le.setText(project_name)
        New.default = False
            
        self.hbox3 = QtGui.QHBoxLayout()
        self.hbox3.addWidget(self.label3)
        self.hbox3.addWidget(self.le)
        
        self.label4 = QtGui.QLabel("    ")
        self.hbox4 = QtGui.QHBoxLayout()
        self.hbox4.addWidget(self.label4)
        
        # Creates OK and cancel buttons
        self.hbox_bottom = QtGui.QHBoxLayout()
        self.hbox_bottom.addStretch()
        self.ok_button = QtGui.QPushButton("OK")
        self.ok_button.clicked.connect(self.getName)
        self.hbox_bottom.addWidget(self.ok_button)
        self.hbox_bottom.addStretch()
        
        self.cancel_button = QtGui.QPushButton("Cancel")
        self.cancel_button.clicked.connect(self.close)
        self.hbox_bottom.addWidget(self.cancel_button)
        self.hbox_bottom.addStretch()

        self.vbox = QtGui.QVBoxLayout()
        self.vbox.addLayout(self.hbox0)
        self.vbox.addLayout(self.hbox1)
        self.vbox.addStretch()
        self.vbox.addLayout(self.hbox2)
        self.vbox.addStretch()
        self.vbox.addLayout(self.hbox3)
        self.vbox.addStretch()
        self.vbox.addLayout(self.hbox4)
        self.vbox.addStretch()
        self.vbox.addLayout(self.hbox_bottom)
        self.setLayout(self.vbox)
        self.show()
    
    def close(self):
        main_win.setEnabled(True)
        super(New, self).close()
        
    # get name and close if enter-key pressed
    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Return :
            self.getName()
        
    def getName(self):
        global project_name
        project_name = self.le.text()
        main_win.setWindowTitle('FutureLearn  - ' + project_name)
        main_win.start_bt.setEnabled(True)
        main_win.view.scene.clear()
        global bt_dict, bcond_dict
        bt_dict = {}
        bcond_dict = {}
        self.close()
############################################################################################################

###########
########### Item class creates polygon items
class Edges(QtGui.QGraphicsItem):

    def __init__(self, pen):
        super(Edges, self).__init__()
        
        self.setFlag(QtGui.QGraphicsItem.ItemIsSelectable)
        self.pen = pen
        self.setZValue(5)
        self.create_line()
        
    def create_line(self, x1=0, y1=0, x2=0, y2=0):
        self.line = QtCore.QLineF(x1 , y1 , x2 , y2)
 
    def shape(self):
        path = QtGui.QPainterPath()
        path.addPolygon(self.boundingRect())
        return path

    def paint(self, painter, option, widget):
        painter.setPen(self.pen)
        
        if self.isSelected ():
            pen = QtGui.QPen(QtCore.Qt.gray, 2, QtCore.Qt.SolidLine, QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin)
            pen.setCosmetic(True)
            painter.setPen(pen)
            boundRect = self.boundingRect ()
            painter.drawRect(boundRect)
        painter.drawLine(self.line)
        
    def setPen(self, color):
        pen = QtGui.QPen(color , 2, QtCore.Qt.SolidLine, QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin)
        pen.setCosmetic(True)
        self.pen = pen

    def boundingRect(self):
        extra = 0 #(self.pen.width()) # + 2) / 2.0
        p1 = self.line.p1()  # QtCore.QPointF(x1,y1) #self.line().p1()
        p2 = self.line.p2()  # QtCore.QPointF(x1,y1) #self.line().p2()
        return QtCore.QRectF(p1, QtCore.QSizeF(p2.x() - p1.x(), p2.y() - p1.y())).normalized().adjusted(-extra, -extra, extra, extra)

#     def boundingRect(self):
#         
#         return self.line.boundingRect()
###########################################################################################################
 
 
###########
########### Item class creates polygon items
class Geometry(QtGui.QGraphicsItem):

    def __init__(self, pen):
        super(Geometry, self).__init__()
        
        self.setFlag(QtGui.QGraphicsItem.ItemIsSelectable)
        self.brush = None
        self.pen = pen
        self.create_rectangle()
        
    def create_rectangle(self, w=0, h=0, x=0, z=0):
        self.xmin = x
        self.xmax = w
        self.ymin = z
        self.ymax = h
        self.polygon = QtGui.QPolygonF([QtCore.QPointF(self.xmax, self.ymax), QtCore.QPointF(self.xmax, self.ymin), QtCore.QPointF(self.xmin, self.ymin), QtCore.QPointF(self.xmin, self.ymax)])       
 
    def shape(self):
        path = QtGui.QPainterPath()
        path.addPolygon(self.polygon)
        return path

    def paint(self, painter, option, widget):
        painter.setPen(self.pen)
        if self.brush:
            painter.setBrush(self.brush)
        
        if self.isSelected ():
            pen = QtGui.QPen(QtCore.Qt.gray, 2, QtCore.Qt.SolidLine, QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin)
            pen.setCosmetic(True)
            painter.setPen(pen)

            boundRect = self.boundingRect ()
            painter.drawRect(boundRect)

        painter.drawPolygon(self.polygon)


    def mousePressEvent(self, event):
        a_scene = QtGui.QGraphicsScene()
        a_scene.mousePressEvent(event)
        # main_win.view.scene.mousePressEvent(event)

    def setBrush(self, brush):
        self.brush = brush

    def boundingRect(self):
        return self.polygon.boundingRect()
    
###########################################################################################################
 
###########  group to gather boundary related items 
############################################################################################################
class Boundary_group(QtGui.QGraphicsItemGroup):
    def __init__(self):
        super(Boundary_group, self).__init__()
        
        self.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, True)


    def paint(self, painter, option, widget):
    
        painter.setRenderHint(QtGui.QPainter.Antialiasing)
        
        brush = QtGui.QBrush(QtGui.QColor("#333333"))
        pen = QtGui.QPen(brush, 0.5)
        pen.setCosmetic(True)
        pen.setStyle(QtCore.Qt.DotLine)
        painter.setPen(pen)
        
        if self.isSelected():
            boundRect = self.boundingRect()
            painter.drawRect(boundRect)
############################################################################################################

 
 
###########
###########  testas    
class Selector(QtGui.QGraphicsItem):

    def __init__(self, point):
        super(Selector, self).__init__()
        self.setZValue(100)
        self.polygon = QtGui.QPainterPath(point)  # QtGui.QPolygonF()
        
    def add_point(self, point):
        self.polygon.lineTo(point)  # .append(point) #QtCore.QPointF(x,y))
        
    def boundingRect(self):
        return self.polygon.boundingRect()
    
    def to_polygon(self):
        self.polygon.closeSubpath()  # toFillPolygon()
    
    def paint(self, painter, option, widget):
        painter.drawPath(self.polygon)
        
    def path(self):
        return self.polygon
#############################################################################################################
    
           
###########     
########### View class , creates a view, scene initialized a scene and integrated to view inside this class
class View(QtGui.QGraphicsView):
    default = True
    def __init__(self):
        super(View, self).__init__()
        self.setRenderHint(QtGui.QPainter.Antialiasing)
  
        self.setWindowTitle("Custom item")
#         self.setViewportUpdateMode(QtGui.QGraphicsView.FullViewportUpdate)
        self.setDragMode(QtGui.QGraphicsView.RubberBandDrag)
        self.scene = QtGui.QGraphicsScene()

#         self.selector = Selector(QtCore.QPointF(12,12))
        self.clickclick = 0
        self.old_selector = None
        
        self.all_group = QtGui.QGraphicsItemGroup()

    def create_domain(self , x=0 , z=0):
        pen = QtGui.QPen(QtCore.Qt.black, 0, QtCore.Qt.SolidLine, QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin)
        self.frame = Geometry(pen)
    
        self.frame.create_rectangle(x, z)
        self.frame.setZValue(1)
        self.frame.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, False)

        self.scene.addItem(self.frame)
        self.scene.mode = "cells"
        self.setScene(self.scene)

#	main_win.view.centerOn(self.frame)
	scale_factor = 	( 500.0 * main_win.view.width() ) / ( x * 778 )
	main_win.view.scale( scale_factor, scale_factor)
	main_win.view.centerOn(self.frame)

        self.line = None
    
    def discretize_domain(self , nb_x=1 , nb_z=1):  
        pen = QtGui.QPen(QtCore.Qt.gray, 0, QtCore.Qt.SolidLine, QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin)                               
        pen.setCosmetic(True)
        self.cell_dict = {}
        self.count = 0       
        dx = domain_x / nb_x
        dz = domain_z / nb_z
        for i in range(0, nb_z):
            self.min_z = i * dz
            self.max_z = (i + 1) * dz
            for j in range(0, nb_x):
                self.min_x = j * dx
                self.max_x = (j + 1) * dx
                cell = Geometry(pen)
                cell.create_rectangle(self.max_x, self.max_z, self.min_x, self.min_z)

                cell.setFlag(QtGui.QGraphicsItem.ItemStacksBehindParent , True)
                cell.setParentItem(self.frame)
                
                self.cell_dict[cell] = [self.count , "button0" , "NaN"]
                self.count += 1
        global cell_dict
        cell_dict = self.cell_dict
        self.setScene(self.scene)

    # as the name suggests...
    def create_edges(self):
        global bcond_dict
        bcond_dict = {}
        Boundaries.default = False
        dx = domain_x / nb_x
        dz = domain_z / nb_z
        pen_line = QtGui.QPen(QtCore.Qt.black, 3, QtCore.Qt.SolidLine, QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin)
        pen_line.setCosmetic(True)
        pen = QtGui.QPen(QtCore.Qt.gray, 1, QtCore.Qt.SolidLine, QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin)                               
        pen.setCosmetic(True)
        pen_arrow_out = QtGui.QPen(QtCore.Qt.red, 1, QtCore.Qt.SolidLine, QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin)
        pen_arrow_out.setCosmetic(True)
        pen_arrow_in = QtGui.QPen(QtCore.Qt.blue, 1, QtCore.Qt.SolidLine, QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin)
        pen_arrow_in.setCosmetic(True)

        # left and right edges
        for j in [0, domain_x]:
            for i in range(0, nb_z):
                if j == 0:
                    boundary = "left"
                    self.min_x = -2 * dx 
                    self.max_x = -1 * dx
                    out = -1
                else:
                    boundary = "right"
                    self.min_x = domain_x + dx
                    self.max_x = domain_x + 2 * dx
                    out = 1
                     
                # Creates a group of items to store  , ghost cell , edge line and arrows
                boundary_group = Boundary_group()
                
                # edge lines
                edge_line = Edges(pen_line)
                edge_line.create_line(j, i * dz, j, (i + 1) * dz)

#                 edge_line.setParentItem(self.frame)
#                 self.scene.addItem(edge_line)

                boundary_group.addToGroup(edge_line)
        
                # Ghost cells
                self.min_z = i * dz
                self.max_z = (i + 1) * dz
                ghost_cell = Geometry(pen)
                ghost_cell.create_rectangle(self.max_x, self.max_z, self.min_x, self.min_z)
                ghost_cell.setVisible(True)
                
#                 self.scene.addItem(ghost_cell)
#                 ghost_cell.setParentItem(self.frame)
                
                boundary_group.addToGroup(ghost_cell)
                
                # Arrow in
                in_arrow_item = Edges(pen_arrow_in)
                in_arrow_item.create_line(j + out * dx, (i + 0.5) * dz, j , (i + 0.5) * dz)
                
#                 self.scene.addItem(in_arrow_item)      
#                 in_arrow_item.setParentItem(self.frame)
                
                boundary_group.addToGroup(in_arrow_item)

                # Arrow out     
                out_arrow_item = Edges(pen_arrow_out)
                out_arrow_item.create_line(j + 2 * out * dx, (i + 0.5) * dz, j + 3 * out * dx, (i + 0.5) * dz)
#                 self.scene.addItem(out_arrow_item)      
                boundary_group.addToGroup(out_arrow_item)   
                
                # creates the dictionary of  boundary conditions
                # Dictionary of boundaries {key : ( right , 5 ) : [ 0 : group , 1 : line , 2 : ghost cell , 3 : arrow in , 4 : arrow out , 5 : h , 6 : q] }
#                 
                bcond_dict[ (boundary  , i + 1) ] = [ boundary_group , edge_line , ghost_cell , in_arrow_item , out_arrow_item , "NaN" , 0  ] 
                
                # Makes group selectable , Adds group to scene
                boundary_group.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, True)
                boundary_group.setParentItem(self.frame)
#                 self.scene.addItem(boundary_group)

        # bottom and top items        
        for j in [0, domain_z]:
            for i in range(0, nb_x):
                if j == 0:
                    boundary = "top"
                    self.min_z = -2 * dz
                    self.max_z = -1 * dz
                    out = -1
                else:
                    boundary = "bottom"
                    self.min_z = domain_z + dz
                    self.max_z = domain_z + 2 * dz
                    out = 1
                    
                # Creates a group of items to store  , ghost cell , edge line and arrows
                boundary_group = Boundary_group()
                
                # edge lines
                edge_line = Edges(pen_line)
                edge_line.create_line(i * dx , j , (i + 1) * dx, j)                
#                 self.scene.addItem(edge_line)
                boundary_group.addToGroup(edge_line)
                                                
                # Ghost cells
                self.min_x = i * dx
                self.max_x = (i + 1) * dx 
                ghost_cell = Geometry(pen)
                ghost_cell.create_rectangle(self.max_x, self.max_z, self.min_x, self.min_z)
#                 self.scene.addItem(ghost_cell)
                boundary_group.addToGroup(ghost_cell)
#                ghost_cell.setVisible(True)

                # Arrow in
                in_arrow_item = Edges(pen_arrow_in)
                in_arrow_item.create_line((i + 0.5) * dx, j + out * dz  , (i + 0.5) * dx, j)
#                 self.scene.addItem(in_arrow_item)      
                boundary_group.addToGroup(in_arrow_item)

                # Arrow out     
                out_arrow_item = Edges(pen_arrow_out)
                out_arrow_item.create_line((i + 0.5) * dx, j + 2 * out * dz  , (i + 0.5) * dx, j + 3 * out * dz)
#                 self.scene.addItem(out_arrow_item)      
                boundary_group.addToGroup(out_arrow_item)   
                
                # creates the dictionary of  boundary conditions
                # Dictionary of boundaries {key : ( right , 5 ) : [ 0 : group , 1 : line , 2 : ghost cell , 3 : arrow in , 4 : arrow out , 5 : h , 6 : q] }
#                 if not bcond_dict:
                if boundary == "bottom":
                    bcond_dict[ (boundary  , i + 1) ] = [ boundary_group , edge_line , ghost_cell , in_arrow_item , out_arrow_item , "NaN" , 0 ]              
                if boundary == "top":
                    bcond_dict[ (boundary  , i + 1) ] = [ boundary_group , edge_line , ghost_cell , in_arrow_item , out_arrow_item , i , "NaN"  ]                  
                
                # Makes group selectable , Adds group to scene
                boundary_group.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, True)
#                 self.scene.addItem(boundary_group)
                boundary_group.setParentItem(self.frame)
                main_win.run_bt.setEnabled(True)
                
    def create_colormaps(self):
        # creates np array with h values in boundaries
        h_matrice = np.array(bcond_dict.values())[:, 5].astype(float)
        # creates np array with h values cells
        h_matrice_2 = np.array(cell_dict.values())[:, 2].astype(float)

        self.legend_item = Legend_bar_item(datasets=[h_matrice, h_matrice_2], title='hydraulic head (m)')

        global color_map_h
        color_map_h = self.legend_item.palette


    def color_boundaries(self):
        self.create_colormaps()
        
        # 
        for key in bcond_dict:
            # if hydraulic head = nan
            if np.isnan(np.float(bcond_dict[key][5])) :
                brush = QtGui.QColor (255 , 255 , 255)
                bcond_dict[key][2].setBrush(brush)
                
                # if flow positive
                if bcond_dict[key][6] > 0:
                    bcond_dict[key][1].setVisible(False)
                    bcond_dict[key][2].setVisible(True)
                    bcond_dict[key][3].setVisible(True)
                    bcond_dict[key][4].setVisible(False)

                # if no flow
                elif bcond_dict[key][6] == 0:
                    bcond_dict[key][1].setVisible(True)
                    bcond_dict[key][2].setVisible(False)
                    bcond_dict[key][3].setVisible(False)
                    bcond_dict[key][4].setVisible(False)

                # if negative flow
                elif bcond_dict[key][6] < 0:    
                    bcond_dict[key][1].setVisible(False)
                    bcond_dict[key][2].setVisible(True)
                    bcond_dict[key][3].setVisible(False)
                    bcond_dict[key][4].setVisible(True)
            
            # if flow is nan
            if np.isnan(np.float(bcond_dict[key][6])) :
                bcond_dict[key][1].setVisible(False)
                bcond_dict[key][3].setVisible(False)
                bcond_dict[key][4].setVisible(False)
                bcond_dict[key][2].setVisible(True)
                
                # gets h value of the cell
                h = bcond_dict[key][5] 
                # creates a brush with the color corresponding to h in this palette
                rgba = color_map_h.to_rgba(h)
                rgba = tuple(np.array(rgba).flatten())
                brush = QtGui.QColor.fromRgbF(rgba[0], rgba[1], rgba[2], rgba[3])  
                                                                       
                bcond_dict[key][2].setBrush(brush)
    
    # Set view panable       
    def set_pan(self):
        self.clickclick = 0
        try:
            main_win.view.scene.removeItem(self.selector)
        except:
            pass
        
        self.selection_mode = "pan"  # selecion_mode = "pan" / "square" / "polygon" controls the behavior of mouseEvents
        self.setCursor(QtCore.Qt.OpenHandCursor)  # cursor = hand

        try:  # inside a try to avoid error if chosen when no frame on scene
            self.all_group.setFlag(QtGui.QGraphicsItem.ItemIsMovable, True)
            self.all_group.addToGroup(self.frame)
            self.scene.addItem(self.all_group)
        except AttributeError:
            pass
    
    # set selection tool rubber-band 
    def set_square(self):
        self.clickclick = 0
        try:
            main_win.view.scene.removeItem(self.selector)
        except:
            pass

        self.selection_mode = "square"  # selecion_mode = "pan" / "square" / "polygon" controls the behavior of mouseEvents
        self.setCursor(QtCore.Qt.ArrowCursor)
        self.scene.clearSelection()
        if self.all_group:
            self.scene.destroyItemGroup(self.all_group)   
            self.all_group = QtGui.QGraphicsItemGroup()

    # set selection tool to selector
    def set_polygon(self):
        self.selection_mode = "polygon"  # selecion_mode = "pan" / "square" / "polygon" controls the behavior of mouseEvents    
        self.setCursor(QtCore.Qt.ArrowCursor)
        self.scene.clearSelection()
        if self.all_group:
            self.scene.destroyItemGroup(self.all_group)   
            self.all_group = QtGui.QGraphicsItemGroup()
            
    # click to draw isolines
#     def set_isoline(self):
#         self.selection_mode = "isoline"   # selecion_mode = "pan" / "square" / "polygon" controls the behavior of mouseEvents    
#         self.setCursor(QtCore.Qt.ArrowCursor)
#         self.scene.clearSelection()
#         self.draw_isolines()
    
    # click to draw pollock's flow lines
    def set_pollock(self):
        self.selection_mode = "pollock"  # selecion_mode = "pan" / "square" / "polygon" controls the behavior of mouseEvents    
        self.setCursor(QtCore.Qt.ArrowCursor)
        self.scene.clearSelection()

    def set_plume(self):
        pipette_px = QtGui.QPixmap('images/pipette.png')
        pipette_px.setMask(pipette_px.mask())
        pipette_cursor = QtGui.QCursor(pipette_px)
        
        self.setCursor(pipette_cursor)
        self.selection_mode = "plume"
        self.scene.clearSelection()


    def assign(self):
        for item in self.scene.selectedItems():
            self.cell_dict[item][1] = selected_bt
            self.recolor()
            self.scene.update()
    
    def keyPressEvent(self, e):
        self.selectedItems = self.scene.selectedItems()
        try:
            if e.key() == QtCore.Qt.Key_Return:

                if self.scene.mode == 'cells':
                    main_win.legend = Legend()
                    main_win.update_legend()
                    self.assign()

                if self.scene.mode == 'boundaries':
                    self.boundaries = Boundaries()
                    self.boundaries.show()   
                    
        except NameError:
            pass
        self.scene.clearSelection()
            

    def mousePressEvent(self, event):
        modifiers = QtGui.QApplication.keyboardModifiers()
        if modifiers == QtCore.Qt.ControlModifier or self.selection_mode == "polygon":
            self._start = self.mapToScene(event.pos())
            if self.clickclick :
                if self.selector in self.scene.items():
                    self.scene.removeItem(self.selector)
                self.selector.add_point(self.mapToScene(event.pos()))
            else :
                self.selector = Selector(self._start)
            self.scene.addItem(self.selector)
            self.clickclick += 1
        elif self.selection_mode == "pollock":
            spoint = self.mapToScene(event.pos())
            x , y = spoint.x() - self.frame.scenePos().x() , spoint.y() - self.frame.scenePos().y()
            self.draw_pollock(x , y)
            pollock_g.print_output(project_name)
            
        elif self.selection_mode == "plume":
            spoint = self.mapToScene(event.pos())
            x , y = spoint.x() - self.frame.scenePos().x() , spoint.y() - self.frame.scenePos().y()
            if x >= 0 and x <= domain_x and y >= 0 and y <= domain_z:
                self.draw_injection(x , y)
                
        else:
            super(View, self).mousePressEvent(event)

    def draw_injection(self, x, y): 
        point = QtCore.QPointF(x,y)

        try:
            main_win.view.scene.removeItem(self.injection_point)
        except AttributeError:
            pass
        pen = QtGui.QPen(QtCore.Qt.red, 7 , QtCore.Qt.SolidLine, QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin)
        pen.setCosmetic(True)
        width = domain_x/2000
        self.injection_point =    QtGui.QGraphicsRectItem(x, y, width, width)
        self.injection_point.setPen(pen)
        self.injection_point.setBrush(QtGui.QBrush(QtGui.QColor("#333333"), QtCore.Qt.SolidPattern))


        self.scene.addItem(self.injection_point)

        self.injection_point.setZValue(10)

        self.injection_point.setParentItem(self.frame)
        
        self.injection = Injection(x,y)
        self.injection.show()

            
    def draw_pollock(self, x, y): 
        point = QtCore.QPointF(x,y)
        if not self.frame.contains(point):
            return
        X, Y, X_marker, Y_marker = pollock_g.pollock_grid(x , y , main_win.win.pollock_time)  # 1000.*3600.)
        X_, Y_, X_marker_, Y_marker_ = pollock_g.pollock_grid(x , y , main_win.win.pollock_time, backwards = True)  # 1000.*3600.)
        
        X, Y = X_[-1:0:-1] + X, Y_[-1:0:-1] + Y

        time = float(pollock_g.output_string.split('\n')[-3].strip().split(';')[-1]) + float(pollock_g.output_string.split('\n')[-2].strip().split(';')[-1])
        pollock_g.output_string2 +='%d , %d , %.2f , %.2f , %.2f , %.2f , %10.4e \n'%(x,y,X_[-1],Y_[-1],X[-1],Y[-1],time)
               
        point = self.frame.mapToItem(self.frame , x , y) 
        self.polygon = QtGui.QPolygonF()
        for i in range(len(X)):
            new_point = QtCore.QPointF(X[i], Y[i])
            self.polygon.append(new_point)
        path = QtGui.QPainterPath()
        path.addPolygon(self.polygon)

        line = QtGui.QGraphicsPathItem()
        line.setZValue(10)
        line.setPath(path)  # , parent = self.frame , scene = self.scene)
        pollock_list.append(line)
        line.setParentItem(self.frame)
        
    def draw_isolines(self, palette, Lx, Ly, nx, ny, h):
        # import data
        f = h5py.File(project_name + '.h5', 'r')  # we open the project file
        self.h_ss = (f[u'output/h_ss'][:, :])  # we pick-up the head matrix and   
        input = f[u'input']
        Lx, Ly = input[u'x'][0, 0], input[u'z'][0, 0]  # length and height of the domain
        nx, ny = input[u'nx'][0, 0], input[u'nz'][0, 0]  # number of columns and rows
        K = input[u'k'][:, :]  # hydraulic conductivity matrix, dim(n_rows,n_cols)
        f.close()
        h = np.reshape(self.h_ss, (ny, nx))
        
        cs = matplotlib.pyplot.contour(np.linspace(Lx / 2. / nx, Lx - Lx / 2. / nx, nx, endpoint=True),
            np.linspace(Ly / 2. / ny, Ly - Ly / 2. / ny, ny, endpoint=True),
            h, 40)
        
        
        coords = [cs.collections[i].get_paths()[0].vertices for i in range(len(cs.collections))]

        for i in range(len(coords)):
            level = cs.levels[i]
            coord = coords[i]
            self.polygon = QtGui.QPolygonF()
            color = palette.to_rgba(level)
            color = QtGui.QColor.fromRgbF(color[0], color[1], color[2], color[3])
            pen = QtGui.QPen(color , 0, QtCore.Qt.SolidLine, QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin)
            for i in range(len(coord)):            
                x , y = coord[i][0] , coord[i][1]        
                new_point = QtCore.QPointF(x , y)
                self.polygon.append(new_point)

                path = QtGui.QPainterPath()
                path.addPolygon(self.polygon)

                line = QtGui.QGraphicsPathItem()
                line.setPen(pen)
                line.setZValue(9)
                line.setPath(path)  # , parent = self.frame , scene = self.scene)
                isolines_list.append(line)
                line.setParentItem(self.frame)
                
    
    def mouseReleaseEvent(self, event):
        modifiers = QtGui.QApplication.keyboardModifiers()
        if modifiers == QtCore.Qt.ControlModifier or self.selection_mode == "polygon":
            if not self.clickclick:
                self.scene.removeItem(self.old_selector)
        else:
            super(View, self).mouseReleaseEvent(event)

    def mouseDoubleClickEvent(self , event):
        modifiers = QtGui.QApplication.keyboardModifiers()
        if modifiers == QtCore.Qt.ControlModifier or self.selection_mode == "polygon":
            self.scene.removeItem(self.selector)
            self.selector.to_polygon()
            self.scene.addItem(self.selector)
            self.old_selector = self.selector
            self.clickclick = 0
            self.scene.setSelectionArea(self.selector.path(), QtCore.Qt.ContainsItemShape)
        else:
            super(View, self).mouseDoubleClickEvent(event)
   

    def recolor(self):
        for key in cell_dict:
            bt_nr = cell_dict[key][1]
            color = bt_dict[bt_nr][2]  
            key.setBrush(color)
            self.update()
            self.scene.update()

            
#     def recolor2(self):#Ben^#   
#         # for all keys in cell dictionary   ## keys are cell items 
#         self.create_colormaps()
#         for key in cell_dict.keys():
#             h = cell_dict[key][2]   
#             rgba = color_map_h.to_rgba(h) 
#             brush = QtGui.QColor.fromRgbF(rgba[0], rgba[1], rgba[2], rgba[3])  
#             key.setBrush(brush)       
        
    def recolor2(self, palette):  # Ben^#   
        # for all keys in cell dictionary   ## keys are cell items 
        # self.create_colormaps()
        for key in cell_dict.keys():
            var = cell_dict[key][2]   
            rgba = palette.to_rgba(var) 
            brush = QtGui.QColor.fromRgbF(rgba[0], rgba[1], rgba[2], rgba[3])  
            key.setBrush(brush)       
            self.update()

        
    def wheelEvent(self, event):    
        self.direction = event.delta() / abs(event.delta())        
        if self.direction < 0 :
            self.scale_factor = 0.9
        elif self.direction > 0 :
            self.scale_factor = 1.1
        self.scale(self.scale_factor, self.scale_factor)
############################################################################################################

#Ben^###########################################################################
class Legend_bar_item(object):
    """This class is to create colorbars to insert in a Legend Widget"""
    def __init__(self, datasets=[], vmax=None, vmin=None, log=False, title=''):
        """datasets is a list of arrays (or iterables) to read for min/max,
        vmin/vmax allow to force the extrema,
        log allows a log scale (default=False),
        title is an optional string to sit at the top of the colorbar."""
        if datasets == [] and (vmax == None or vmin == None): return
        # the previous test stops the call if too little info is given
        self.vmin, self.vmax, self.datasets, self.log = vmin, vmax, datasets, log
        self.title = title
        self.update()

    def update(self):
        """After init you can change vmin/vmax or other attributes and then call
        this update method to have all attributes consistent."""
        if self.datasets != []:  # this converts to np-types and flattens all dsets
            self.datasets = [np.array(dset).flatten() for dset in self.datasets]
        
#         print [np.nanmax(d) for d in self.datasets] , "##############"
        # this affects vmin/vmax if explicitly specified, else from datasets
        if self.vmax != None: self.vmax = float(self.vmax)
        else: self.vmax = np.nanmax(np.array([np.nanmax(d) for d in self.datasets]))
        if self.vmin != None: self.vmin = float(self.vmin)
        else: self.vmin = np.nanmin(np.array([np.nanmin(d) for d in self.datasets]))
        
        # this creates the matplotlib objects
        if self.vmin <= 0.: self.log = False  # for safety, not possible
        if not self.log: self.norm = matplotlib.colors.Normalize(self.vmin, self.vmax)
        elif self.log: self.norm = matplotlib.colors.LogNorm(self.vmin, self.vmax)
        self.palette = matplotlib.cm.ScalarMappable(norm=self.norm, cmap=CMAP)
################################################################################

#Ben^###########################################################################
class Legend(QtGui.QWidget):
    """This class is to create a Legend Widget to insert next to the view"""
    def __init__(self, parent=None):  # standard 
        super(Legend, self).__init__()
        self.items = []
        self.setLayout(QtGui.QVBoxLayout())
        self.fig = Figure((1., 6.))
        combobox = QtGui.QComboBox()
        self.layout().addWidget(combobox)
        self.layout().addWidget(FigureCanvas(self.fig))
        combobox.currentIndexChanged.connect(self.update)
        
    def addItem(self, item):
        self.items.append(item)
        self.layout().itemAt(0).widget().addItem(item.title)
        self.update()
        
    def update(self):
        item = self.items[self.layout().itemAt(0).widget().currentIndex()]
        item.update()
        self.fig.clf()
        ax = self.fig.add_subplot(111)
        cb = matplotlib.colorbar.ColorbarBase(ax, orientation='vertical',
                                 cmap=item.palette.cmap, norm=item.palette.norm)
        self.fig.subplots_adjust(left=0.05, bottom=0.05, right=0.4, top=0.95)
        self.layout().itemAt(1).widget().setParent(None) 
        self.layout().addWidget(FigureCanvas(self.fig))
################################################################################


############### Widget for injection
class Injection(QtGui.QWidget):
    default = True
    
    def __init__(self , x , y):
        super(Injection, self).__init__()
        
        self.x = x
        self.y = y
        # calls initUI to design widget    
        self.initUI()  
        
    def initUI(self):      
        # create first row : 1 line edit
        self.hbox_Dx = QtGui.QHBoxLayout()
        self.Dx_label = QtGui.QLabel("injection name")
        self.le_Dx = QtGui.QLineEdit("injection_1") 
        self.hbox_Dx.addWidget(self.Dx_label)
        self.hbox_Dx.addWidget(self.le_Dx)
        
        # create first row : 2 line edit
        self.hbox_Dy = QtGui.QHBoxLayout()
        self.Dy_label = QtGui.QLabel("D (m2/s): ")
        self.le_Dy = QtGui.QLineEdit("1e-6")        
        self.hbox_Dy.addWidget(self.Dy_label)
        self.hbox_Dy.addWidget(self.le_Dy)
           
        # create first row : 2 line edit with time
        self.hbox_times = QtGui.QHBoxLayout()
        self.times_label = QtGui.QLabel("Obervation times (s) : ")
        self.le_times = QtGui.QLineEdit("1.0e8, 2.0e8, 3.0e8, 4.0e8, 5.0e8")        
        self.hbox_times.addWidget(self.times_label)
        self.hbox_times2 = QtGui.QHBoxLayout()
        self.hbox_times2.addWidget(self.le_times)        
        
        self.vbox = QtGui.QVBoxLayout(self) 
 
        # Creates OK and cancel buttons
        self.hbox_bottom = QtGui.QHBoxLayout()
        self.strunt = QtGui.QLabel("  ")
        self.vspace = QtGui.QHBoxLayout()
        self.vspace.addWidget(self.strunt)   
        self.hbox_bottom.addStretch()
        self.ok_button = QtGui.QPushButton("OK")
        
        self.ok_button.clicked.connect(self.getValues)
        self.hbox_bottom.addWidget(self.ok_button)
        self.hbox_bottom.addStretch()
    
        self.cancel_button = QtGui.QPushButton("Cancel")
        self.cancel_button.clicked.connect(self.close)
        self.hbox_bottom.addWidget(self.cancel_button)
        self.hbox_bottom.addStretch()
        
        # places horizontal layouts in the vertical one
        self.vbox.addLayout( self.hbox_Dx )
        self.vbox.addLayout( self.hbox_Dy )
        self.vbox.addLayout( self.hbox_times )
        self.vbox.addLayout( self.hbox_times2 )
        self.vbox.addLayout(self.hbox_bottom)
        
    # Tjeena mannen  *ALTRUISME*
    # yo ben , c est ici qu'on viens cehrcher les variables *altruisme*
    # t es pas obligé de les gardé en global si tu veux les passer en argument...
    def getValues(self):   
        try :  # try , avoids non-floats to be read
            global inj_name , D, time_list, injection_x, injection_y
 
            injection_x = self.x ; injection_y = self.y
            D = np.float(self.le_Dy.text()) 
            inj_name = str(self.le_Dx.text())
            time_list = self.le_times.text().split(",")
            time_list = np.array(time_list)
            time_list = list(time_list.astype(float))
            
            # close widget            
            self.close()
            try: os.mkdir(inj_name)
            except: pass
            os.chdir(inj_name)
            plt.close()#precaution
            inject.solve_injection("../"+project_name+".h5",injection_x,injection_y,D,time_list,inj_name)
            os.chdir('..')
        except ValueError:
            QtGui.QMessageBox.warning(self, "", "please enter floats!") 
#########################################################################################











#Ben^###########################################################################
class Visu(QtGui.QWidget):
    """This class is a widget to show the list of possibilities to visualize at
    post processing and manage the selection process"""
    def __init__(self, window, parent=None):  # this is quite standard
        super(Visu, self).__init__()
        self.window = window
        
        # import data
        f = h5py.File(project_name + '.h5', 'r')  # we open the project file
        self.h_ss = (f[u'output/h_ss'][:, :])  # we pick-up the head matrix and
        self.ggh_ss = (f[u'output/ggh_ss'][:, :])  # the cell-centered gradient
        self.k = (f[u'input/k'][:, :])  # the hydraulic conductivity
        self.phi = (f[u'input/phi'][:, :])  # the porosity
        
        input = f[u'input']
        self.Lx, self.Ly = input[u'x'][0, 0], input[u'z'][0, 0]  # length and height of the domain
        self.nx, self.ny = input[u'nx'][0, 0], input[u'nz'][0, 0]  # number of columns and rows
        K = input[u'k'][:, :]  # hydraulic conductivity matrix, dim(n_rows,n_cols)
        h_left = input[u'h_left'][:,0]#fixed head on the left boundary
        h_right = input[u'h_right'][:,0]#fixed head on the right boundary
        h_top = input[u'h_top'][0,:]#fixed head on the top boundary
        h_bottom = input[u'h_bottom'][0,:]#fixed head on the bottom boundary
        q_left = input[u'q_left'][:,0]#fixed head gradient on the left boundary
        q_right = input[u'q_right'][:,0]#fixed head gradient on the right boundary
        q_top = input[u'q_top'][0,:]#fixed head gradient on the top boundary
        q_bottom = input[u'q_bottom'][0,:]#fixed head gradient on the bottom bndry
        f.close()
         
        self.h = np.reshape(self.h_ss, (self.ny, self.nx))
        norm = np.sqrt(self.ggh_ss[0, :, :] ** 2 + self.ggh_ss[1, :, :] ** 2)
        norm = np.reshape(norm, (self.ny, self.nx))
        k = np.reshape(self.k, (self.ny, self.nx))
        phi = np.reshape(self.phi, (self.ny, self.nx))
        
        # calculates a constant to determine an optimum time for pollock
        inverse_speed = phi / (K * norm)
        inverse_speed_max = np.max(inverse_speed.flatten())
        d_visu = 0.2
        self.pollock_time = d_visu * inverse_speed_max
        self.pollock_time = 3600.*24.*365.*10.###delme

        # create grid
        global pollock_g
        pollock_g = grid_pollock2D (self.Lx, self.Ly, self.nx, self.ny)
        pollock_g.sethphiK(self.h, phi/100., K)
        pollock_g.setboundaries(h_bottom,q_bottom,h_top,q_top,h_left,q_left,h_right,q_right)
        
        self.initUI()
        
    def initUI(self):
        
        # background color plots
        self.bkgnd_cbox = QtGui.QCheckBox('Background')
        self.bkgnd_cbox.setChecked(True)
        self.bkgnd_cbox.clicked.connect(self.on_check)
        bkgnd_btgroup = QtGui.QButtonGroup()
        self.bkgnd_head = QtGui.QRadioButton("Hydraulic Head (m)")
        self.bkgnd_head.dataset = self.h_ss
        # update the legend
        self.window.legend = Legend()
        h_bnd = np.array(bcond_dict.values())[:, 5].astype(float)
        self.window.legend.addItem(Legend_bar_item(datasets=[self.h_ss, h_bnd], title='head (m)'))
#         self.window.legend.items[0].datasets=[self.h_ss, h_bnd]
#         self.window.legend.items[0].update()
        self.bkgnd_head.palette = self.window.legend.items[-1].palette
        self.bkgnd_head.clicked.connect(self.on_bkgnd_item_select)
        bkgnd_btgroup.addButton(self.bkgnd_head)
        bkgnd_cond = QtGui.QRadioButton("Hydraulic Conductivity (m/s)")
        bkgnd_cond.dataset = self.k
        self.window.legend.addItem(Legend_bar_item(datasets=[self.k], title='cond. (m/s)', log=True))
        bkgnd_cond.palette = self.window.legend.items[-1].palette
        bkgnd_cond.clicked.connect(self.on_bkgnd_item_select)
        bkgnd_btgroup.addButton(bkgnd_cond)
        bkgnd_poro = QtGui.QRadioButton("Porosity (%)")
        bkgnd_poro.dataset = self.phi
        self.window.legend.addItem(Legend_bar_item(datasets=[self.phi], title='porosity (%)'))
        bkgnd_poro.palette = self.window.legend.items[-1].palette
        bkgnd_poro.clicked.connect(self.on_bkgnd_item_select)
        bkgnd_btgroup.addButton(bkgnd_poro)
        self.bkgnd_cbox.group = bkgnd_btgroup
        self.window.update_legend()  # update legend
        
        # iso-line plots
        self.iso_cbox = QtGui.QCheckBox('Countour lines')
        self.iso_cbox.setChecked(True)
        print self.nx, self.ny

        if self.nx > 1 and self.ny > 1 :
            main_win.view.draw_isolines(self.bkgnd_head.palette, self.Lx, self.Ly, self.nx, self.ny, self.h)

#         main_win.isoline.setEnabled(True)
        self.iso_cbox.clicked.connect(self.on_iso)
        
#         iso_btgroup = QtGui.QButtonGroup()
#         iso_head = QtGui.QRadioButton("Hydraulic Head (m)")
#         iso_head.dataset = None  ###to be cont'ued
#         iso_btgroup.addButton(iso_head)
#         iso_cbox.group = iso_btgroup
        
        # flow-line plots
        self.flow_cbox = QtGui.QCheckBox('Flow lines')
        self.flow_cbox.setChecked(True)
        main_win.pollock.setEnabled(True)
        main_win.plume.setEnabled(True)
        self.flow_cbox.clicked.connect(self.on_pollock)
        self.flowstat_bton = QtGui.QPushButton('Travel time statistics')
        self.flowstat_bton.clicked.connect(self.on_flowstat)
#         flow_cbox.clicked.connect(self.on_check)
#         flow_btgroup = QtGui.QButtonGroup()
#         flow_line = QtGui.QRadioButton("Flow lines")
#         flow_line.clicked.connect( self.on_pollock)
#         flow_line.dataset=None###to be cont'ued
#         flow_btgroup.addButton(flow_line)
#         flow_cbox.group = flow_btgroup

#         # vector plots
#         vect_cbox = QtGui.QCheckBox('Vector fields')
#         vect_cbox.setChecked(True)
#         vect_cbox.clicked.connect(self.on_check)
#         vect_btgroup = QtGui.QButtonGroup()
#         vect_disc = QtGui.QRadioButton("Discharge (m/s)")
#         vect_disc.dataset = None  # ##to be cont'ued
#         vect_btgroup.addButton(vect_disc)
#         vect_velo = QtGui.QRadioButton("Particle Velocity (m/s)")
#         vect_velo.dataset = None  # ##to be cont'ued
#         vect_btgroup.addButton(vect_velo)
#         vect_cbox.group = vect_btgroup        
        # fixing the layout
        grid = QtGui.QGridLayout()
        grid.addWidget(self.bkgnd_cbox, 0, 0, 1, 5)
        grid.addWidget(self.bkgnd_head, 1, 1, 1, 5)
        grid.addWidget(bkgnd_cond, 2, 1, 5)
        grid.addWidget(bkgnd_poro, 3, 1, 5)
        grid.addWidget(self.iso_cbox, 4, 0, 1, 5)
#         grid.addWidget(iso_head,5,1,1,5)
        grid.addWidget(self.flow_cbox, 6, 0, 1, 5)
#         grid.addWidget(flow_line,7,1,1,5)
        grid.addWidget(self.flowstat_bton,7,1,1,5)
#         grid.addWidget(vect_cbox, 8, 0, 1, 5)
#         grid.addWidget(vect_disc, 9, 1, 1, 5)
#         grid.addWidget(vect_velo, 10, 1, 1, 5)

        self.setLayout(grid)

        img = os.path.join(WORK_DIRECTORY,"images","water.png")        
#        img = resource_path("images/water.png")
#         img = os.path.relpath("images/water.png", "./")
        self.setWindowIcon(QtGui.QIcon(img))


    def on_check(self):
        if self.sender().checkState() == QtCore.Qt.CheckState.Checked:
            for item in self.sender().group.buttons(): item.setEnabled(True)
        else:
            for item in self.sender().group.buttons(): item.setEnabled(False)
            if self.sender() == self.bkgnd_cbox:
                self.window.view.recolor()
                
            
    def on_pollock(self):
        if self.flow_cbox.isChecked():
            main_win.pollock.setEnabled(True)
            for item in pollock_list:
                item.setVisible(True)
        else:
            main_win.pollock.setEnabled(False)
            for item in pollock_list:
                item.setVisible(False)
                
                
    def on_flowstat(self):
        times = pollock_g.get_travel_times()
        
        if len(times)==0:
            QtGui.QMessageBox.information( self, "no statistics to display" , """Create flow lines first to collect their statistics. \n \t Use the "P" tool in toolbar! """ )

            return
        else:
            self.stats = Stats()
            self.stats.show()
            
            #fig = matplotlib.pyplot.figure()
#             matplotlib.pyplot.subplot(211)
#             matplotlib.pyplot.hist(times)
#             matplotlib.pyplot.ylabel('# of trajectories')           
#             matplotlib.pyplot.xlabel('time (s)')
#             matplotlib.pyplot.subplot(212)
#             matplotlib.pyplot.boxplot(times,vert=0)
#             matplotlib.pyplot.show()
#                 
    def on_iso(self):
        if self.iso_cbox.isChecked():
                for line in isolines_list:
                    line.setVisible(True)
        else:
            for line in isolines_list:
                line.setVisible(False)      

    def on_bkgnd_item_select(self):
        dataset = self.sender().dataset.flatten()
        for item in cell_dict.keys():  # for each cell in cell_dict we append
            cell_dict[item][2] = dataset[cell_dict[item][0]]  # the value of dataset
        self.window.view.recolor2(self.sender().palette)  # then we recolor the cells
        self.window.view.color_boundaries()
        self.window.view.setScene(self.window.view.scene)
        self.window.view.scene.update()
################################################################################                    


######################   Widget for  flowstats
class Stats(QtGui.QWidget):
    default = True
    
    def __init__(self , parent=None):
        super(Stats, self).__init__()    
        self.initUI()  
        
    def initUI(self):      
     
        # creates layout boxes
        self.hbox_bottom = QtGui.QHBoxLayout()
        self.hbox_middle = QtGui.QHBoxLayout()
        self.hbox_top = QtGui.QHBoxLayout()
        self.vbox = QtGui.QVBoxLayout(self) 

        # creates canva with stats and addsa it to self.hbox_top
        times = pollock_g.get_travel_times()
        print type(times)
        print times
        print np.nanmin(times)
        print np.nanmax(times)
        fig = matplotlib.pyplot.figure()
        fig.add_subplot(211)
        matplotlib.pyplot.hist(times)
        matplotlib.pyplot.ylabel('# of trajectories')           
        fig.add_subplot(212)
        matplotlib.pyplot.boxplot(times,vert=0)
        matplotlib.pyplot.xlabel('time (s)')
        canvas = FigureCanvas(fig)        
        self.hbox_top.addWidget(canvas)
              
        # create text edit and adds it to self.hbox_middle
        self.tedit = QtGui.QTextEdit()
        text = pollock_g.output_string2
#         *benediction*
        #'Clicked point X (m), Clicked point Y (m), Start point X (m); Start point Y (m), End point X (m), End point Y (m), Travel time (s)\n'

        self.tedit.insertPlainText(text)
        self.hbox_middle.addWidget(self.tedit)
        
        # Creates OK button
        self.hbox_bottom.addStretch()
        self.ok_button = QtGui.QPushButton("OK")   
        self.ok_button.clicked.connect(self.close)
        self.hbox_bottom.addWidget(self.ok_button)
        self.hbox_bottom.addStretch()
        
        # places horizontal layouts in the vertical one
        self.vbox.addLayout(self.hbox_top)
        self.vbox.addLayout(self.hbox_middle)
        self.vbox.addLayout(self.hbox_bottom)

############
############   Main window
class Window(QtGui.QMainWindow):
    default = True
    
    def __init__(self):
        super(Window, self).__init__()
        if Window.default:
            global project_name, pollock_list, isolines_list
            isolines_list = []
            pollock_list  = []
            project_name = "Untitled"
        Window.default = False
            
        self.initUI()
        
    def initUI(self):
        self.view = View()   
        self.menu()

        # Create Buttons
        self.prestart_bt = QtGui.QPushButton('Start', self)
        self.prestart_bt.setEnabled(True)
        self.prestart_bt.setToolTip("<br>  <b> Create a new file to initiate problem \n Use menu bar to open an existing file </b>  <br>")


        self.start_bt = QtGui.QPushButton('Domain', self)
        self.start_bt.setEnabled(False)
        self.start_bt.setToolTip("<br>  <b> Define domain's dimensions </b>  <br>")


        self.mesh_bt = QtGui.QPushButton('Mesh', self)
        self.mesh_bt.setEnabled(False)
        self.mesh_bt.setToolTip("<br>  <b> Define grid resolution </b>  <br>")

        self.props_bt = QtGui.QPushButton('Properties', self)
        self.props_bt.setEnabled(False)
        self.props_bt.setToolTip("<br>  <b> Edit material properties </b>  <br>")

        self.boundaries_bt = QtGui.QPushButton('Boundaries', self)
        self.boundaries_bt.setEnabled(False)
        self.boundaries_bt.setToolTip("<br>  <b> Initiate / edit boundary conditions </b>  <br>")

        self.run_bt = QtGui.QPushButton('Run', self)
        self.run_bt.setEnabled(False)
        self.run_bt.setToolTip("<br>  <b> Solve the equation </b>  <br>")

        self.visualization_bt = QtGui.QPushButton('Visualization', self)  # Ben^#
        self.visualization_bt.setEnabled(False)  # Ben^#

        # Connect Buttons with actions
        self.prestart_bt.clicked.connect(self.on_new)
        self.start_bt.clicked.connect(self.on_start)
        self.mesh_bt.clicked.connect(self.on_mesh)
        self.props_bt.clicked.connect(self.on_props)
        self.boundaries_bt.clicked.connect(self.on_boundaries)
        self.run_bt.clicked.connect(self.on_run)
        self.visualization_bt.clicked.connect(self.on_visualize)  # Ben^#
        self.visualization_bt.setToolTip("<br>  <b> Post-processing </b>  <br>")
            
        # Creates a horizontal box with buttons at the start and stretch at the end
        hbox = QtGui.QHBoxLayout()
        hbox.addWidget(self.prestart_bt)
        hbox.addWidget(self.start_bt)
        hbox.addWidget(self.mesh_bt)
#         hbox.addWidget(self.table_bt)
        hbox.addWidget(self.props_bt)
        hbox.addWidget(self.boundaries_bt)
        hbox.addWidget(self.run_bt)
        # Ben^#hbox.addWidget(self.flow_bt)
        hbox.addWidget(self.visualization_bt)  # Ben^#
#         hbox.addWidget(self.animation_bt)
        hbox.addStretch(1)
        
        # Creates a toolbar with  pan / selection tools / zoom?
        hbox_2 = QtGui.QHBoxLayout()
        hbox_2.addWidget(self.view)
        
        self.hbox_2 = hbox_2
        self.legend = Legend()
        hbox_2.addWidget(self.legend)
        self.hbox_2 = hbox_2
            
        self.tool_bar = QtGui.QToolBar()
        self.tool_bar.setMovable(True)
        
        self.view.selection_mode = "square"
        self.pan = QtGui.QToolButton()
##      img = resource_path(["images","hand.png"])
        img = os.path.join(WORK_DIRECTORY,"images","hand.png")
##        img = os.path.relpath("images/hand.png", "./")
        self.pan.setIcon(QtGui.QIcon(img))
        self.pan.setCheckable(True)
        self.pan.clicked.connect(self.view.set_pan)
        self.pan.setToolTip('<br>  <b>Pan</b>  <br>')

        self.square = QtGui.QToolButton()
        img = os.path.join(WORK_DIRECTORY,"images","square.png")
#        img = resource_path("images/square.png")
#         img = os.path.relpath("images/square.png", "./")
        self.square.setIcon(QtGui.QIcon(img))
        self.square.setCheckable(True)        
        self.square.clicked.connect(self.view.set_square)
        self.square.setToolTip('<br>  <b> Rubber-band selection tool</b>  <br>')

        self.polygon = QtGui.QToolButton()
        img = os.path.join(WORK_DIRECTORY,"images","polygon.png")
#        img = resource_path("images/polygon.png")
#         img = os.path.relpath("images/polygon.png" , "./")
        self.polygon.setIcon(QtGui.QIcon(img))
        self.polygon.setCheckable(True) 
        self.polygon.clicked.connect(self.view.set_polygon)
        self.polygon.setToolTip('<br>  <b>Polygon selection tool </b>  <br>')
        
        self.pollock = QtGui.QToolButton()
        img = os.path.join(WORK_DIRECTORY,"images","pollock.png")
#        img = resource_path("images/pollock.png")
#         img = os.path.relpath("images/pollock.png" , "./")
        self.pollock.setIcon(QtGui.QIcon(img))
        self.pollock.setCheckable(True) 
        self.pollock.clicked.connect(self.view.set_pollock)
        self.pollock.setEnabled(False)
        self.pollock.setToolTip('<br>  <b> Pollock flow lines tool </b>  <br>')

        self.plume = QtGui.QToolButton()
        img = os.path.join(WORK_DIRECTORY,"images","plume.png")
#        img = resource_path("images/plume.png")
#         img = os.path.relpath("images/plume.png" , "./")
        self.plume.setIcon(QtGui.QIcon(img))
        self.plume.setCheckable(True) 
        self.plume.clicked.connect(self.view.set_plume)
        self.plume.setEnabled(False)
        self.plume.setToolTip('<br>  <b> Tracer injection tool </b>  <br>')
                
#         if not self.pollock.isEnabled():
# #             self.pollock.setToolTip('<br>  <b> Pollock flow lines tool </b> (disabled) <br>')
        
#         self.isoline = QtGui.QToolButton()
#         img = os.path.relpath("images/isolines.png" , "./")
#         self.isoline.setIcon(QtGui.QIcon(img))
#         self.isoline.setCheckable(True) 
#         self.isoline.clicked.connect(self.view.set_isoline)
#         self.isoline.clicked.connect(self.view.set_isoline)
#         self.isoline.setEnabled(False)
#         self.isoline.setToolTip('<br>  <b> Isohead lines tool </b>  <br>')
                
        self.bt_group = QtGui.QButtonGroup()
        self.bt_group.addButton(self.pan)   
        self.bt_group.addButton(self.square)   
        self.bt_group.addButton(self.polygon)  
        self.bt_group.addButton(self.pollock)
        self.bt_group.addButton(self.plume)
#         self.bt_group.addButton(self.isoline)
        self.bt_group.setExclusive(True)       

        self.tool_bar.addWidget(self.pan)
        self.tool_bar.addWidget(self.square)
        self.tool_bar.addWidget(self.polygon)        
        self.tool_bar.addWidget(self.pollock)    
        self.tool_bar.addWidget(self.plume) 
#         self.tool_bar.addWidget(self.isoline)      
        self.addToolBar(self.tool_bar)
        
        # create a vertical box with hbox at the top and stretch at the bottom
        self.vbox = QtGui.QVBoxLayout()
        self.vbox.addLayout(hbox)
        self.vbox.addLayout(hbox_2)
#         self.vbox.addWidget(self.view)
                
        # Puts vbox in widget
        self.main_widget = QtGui.QWidget()
        self.main_widget.setLayout(self.vbox)
        
        # Integrate main widget in window
        self.setCentralWidget(self.main_widget)
        
        # Defines window position and size
        self.setGeometry(100, 100, 900, 600)
        self.setWindowTitle('FutureLearn  - ' + project_name)
        img = os.path.join(WORK_DIRECTORY,"images","water.png")
#        img = resource_path("images/water.png")
#         img = os.path.relpath("images/water.png", "./")
        self.setWindowIcon(QtGui.QIcon(img))
        
        self.firststart = 0
        
    def menu(self):               
        self.newAction = QtGui.QAction('&New', self)
        self.newAction.setShortcut('Ctrl+N')
        self.newAction.triggered.connect(self.on_new)
        
        self.openAction = QtGui.QAction('&Open', self)
        self.openAction.setShortcut('Ctrl+O')
        self.openAction.triggered.connect(self.open_dialog)
        
        self.saveAction = QtGui.QAction('&Save', self)
        self.saveAction.setShortcut('Ctrl+S')
        self.saveAction.triggered.connect(self.save_dialog)
        
        self.saveAsAction = QtGui.QAction('&Save as', self)
        self.saveAsAction.setShortcut('Ctrl+Shift+S')
        self.saveAsAction.triggered.connect(self.save_as_dialog)
        
        self.exitAction = QtGui.QAction('&Exit', self)
        self.exitAction.setShortcut('Ctrl+Q')
        self.exitAction.triggered.connect(self.close)
        
        self.confinedAction = QtGui.QAction('&Confined', self)
        
        
        self.menubar = self.menuBar()
        self.fileMenu = self.menubar.addMenu('&File')
        self.fileMenu.addAction(self.newAction)
        self.fileMenu.addAction(self.openAction)
        self.fileMenu.addAction(self.saveAction)
        self.fileMenu.addAction(self.saveAsAction)
        self.fileMenu.addAction(self.exitAction)
        
        
#         self.modeMenu = self.menubar.addMenu('&Mode')
#         self.modeMenu.addAction(self.confinedAction)

    def save_dialog(self):
        self.fname = project_name + ".h5"
        self.save(self.fname)
        
    def save_as_dialog(self):
        self.fname, _ = QtGui.QFileDialog.getSaveFileName(self, 'Open file', os.curdir, "*.h5")
        self.setWindowTitle(os.path.basename(self.fname))
        if not self.fname[-3:] == ".h5":
            self.fname += ".h5"
        self.save(self.fname)

    def open_dialog(self):
        self.view.scene = QtGui.QGraphicsScene()
        self.fname, _ = QtGui.QFileDialog.getOpenFileName(self, 'Open file', os.curdir, "*.h5")
        if self.fname == " ":
            return
        global project_name
        project_name = os.path.basename(self.fname)[:-3]
        self.setWindowTitle('FutureLearn  - ' + project_name)
        self.load(self.fname)
        
    def load(self, filename):
        # opens selected file
        f = h5py.File(filename)
        self.start_bt.setEnabled(True)
        
        # loads domain's dimensions
        try:
            global domain_x, domain_z 
            domain_x = float(f["/input/x"][0])
            domain_z = float(f["/input/z"][0])
            self.view.create_domain(domain_x, domain_z)
            self.mesh_bt.setEnabled(True)
            Mesh.default = False
        except KeyError:
            pass

        
        # loads mesh values
        try:        
            global nb_x, nb_z  # , k_matrix, value_matrix
            nb_x = int(f["/input/nx"][0])
            nb_z = int(f["/input/nz"][0])
            self.view.discretize_domain(nb_x , nb_z)
            self.props_bt.setEnabled(True)    
            self.boundaries_bt.setEnabled(True)  
            Props.default = False     
        except KeyError:
            pass
        
        # loads cells' values , and button values 
        try:
            global bt_dict
            bt_dict = {}
            dataset = f["/input/bt_dict"] 

            # loads button values and set values in bt_dict
            for row in dataset:
                color = QtGui.QColor(row[3])
                bt_dict[row[0]] = [ float(row[1]) , float(row[2]) , color , 0 , 0 , 0 , str(row[7])]
            self.boundaries_bt.setEnabled(True)   
                            
            # loads cell values and set values in cell_dict                            
            h5_cell_dic = {}
            h5_cells = list(f["/input/cell_dict"])
            # creates a help dictionary with cell id as keys
            for row in h5_cells:
                h5_cell_dic[int(row[0])] = row[1] 
            # goes through cell items and puts bt number in their dictionary definition
            for item in cell_dict:
                id = cell_dict[item][0]
                bt = h5_cell_dic[id]
                cell_dict[item][1] = bt
            self.view.recolor()
        except KeyError:
            pass
        
        # Loads boundary conditions
        try:
            h_values = f["/input/h_top"][0]
            
            global bcond_dict
            bcond_dict = {}
            self.view.create_edges()
    
            # generates 4 data sets
            for boundary in [ "top" , "bottom" ]:  
                h_values = f["/input/h_%s" % boundary][0]
                q_values = f["/input/q_%s" % boundary][0]
                for i in range(0 , nb_x):
                    key = (boundary , i + 1)
                    bcond_dict[key][5] = h_values[i]
                    bcond_dict[key][6] = q_values[i]
                    
            for boundary in [ "left" , "right" ]:  
                h_values = f["/input/h_%s" % boundary]
                q_values = f["/input/q_%s" % boundary]
    
                for i in range(0 , nb_z):
                    key = (boundary , i + 1)
                    bcond_dict[key][5] = h_values[i]
                    bcond_dict[key][6] = q_values[i]
            
            #  Decides over what should be selectable and how return should behave
            main_win.view.scene.mode = "boundaries"
            for item in cell_dict:
                item.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, False)
            for item in bcond_dict:
                bcond_dict[item][0].setFlag(QtGui.QGraphicsItem.ItemIsSelectable, True)
    
    
            main_win.view.color_boundaries()
            
        except KeyError:
            pass

        f.close()
#         self.set_scene(self.view)
        
    def save(self, filename):
        # Deletes previous file with same name if it exists
        try:
            os.remove(filename)
        except OSError:
            pass
        
        # opens a h5 file
        h5file = h5py.File(filename)
        # creates Group in h5
        input_group = h5file.create_group("input")

        # Stores Domain's dimensions
        try :
            dataset_x = input_group.create_dataset("x", (1, 1), "f")
            dataset_x[:] = domain_x
            dataset_z = input_group.create_dataset("z", (1, 1), "f")
            dataset_z[:] = domain_z
        except NameError:
            h5file.close()
            return 
        
        # Stores Grid resolution data   
        try :
            dataset_nx = input_group.create_dataset("nx", (1, 1), "i")
            dataset_nx[:] = nb_x
            dataset_nz = input_group.create_dataset("nz", (1, 1), "i")
            dataset_nz[:] = nb_z
        except NameError :
            h5file.close()
            return
        
        # writes a matrix of k and phi
        # writes cell and button information for scene setup
        try:
            global k_values
            k_values = [None] * nb_x * nb_z
            phi_values = [None] * nb_x * nb_z
    
            for cell in cell_dict:
                id = cell_dict[cell][0]
                button_nr = cell_dict[cell][1]
                k = bt_dict[button_nr][0]
                phi = bt_dict[button_nr][1]
                bt_name = bt_dict[button_nr][6]
                k_values[id] = k
                phi_values[id] = phi
            
            k_values = np.array(k_values).reshape(nb_z, nb_x)
            phi_values = np.array(phi_values).reshape(nb_z, nb_x)
            
            h5file["/input/k"] = k_values
            h5file["/input/phi"] = phi_values
                
            # partially writes bt_dict dictionary of buttons an related values into h5
            column = []
            for key in bt_dict: 
                row = [key , bt_dict[key][0], bt_dict[key][1], str(bt_dict[key][2].name()) , 0 , 0 , 0 , bt_dict[key][6]]  # .toTuple()[0] , bt_dict[key][2].toTuple()[1] , bt_dict[key][2].toTuple()[2] , bt_dict[key][2].toTuple()[3] ]
                #     [  key  ,  k          ,       phi       ,         "button_nr"         , 0 , 0 , 0 ] 
                column.append(row)
            column = np.array(column)
            h5file["/input/bt_dict"] = column
            
            # writes values (not entries) of cell_dict dictionary of cells into h5
            cells = []
            for key, values in cell_dict.iteritems():
                cells.append(values)
            cells = np.array(cells)
            h5file["/input/cell_dict"] = cells
        except NameError:
            pass
        
        # Writes boundary conditions into 8 vectors R, L, T, B  for q and h
#         try:
            # generates 4 data sets

        for boundary in ["right" , "left", "top" , "bottom"]:  
            if boundary == "right" or boundary == "left" :
                h_values = [None] * nb_z
                q_values = [None] * nb_z
            else:
                h_values = [None] * nb_x
                q_values = [None] * nb_x
            
            for key in bcond_dict.keys():
                if boundary == key[0] :
                    id = key[1]
                    h = np.float(bcond_dict[key][5])
                    q = np.float(bcond_dict[key][6])
                    h_values[id - 1] = h
                    q_values[id - 1] = q      

            if boundary == "right" or boundary == "left" :
                h_values = np.array(h_values).reshape(nb_z, 1)
                q_values = np.array(q_values).reshape(nb_z, 1)
            else:
                h_values = np.array(h_values).reshape(1, nb_x)
                q_values = np.array(q_values).reshape(1, nb_x)
            
            h5file["/input/h_%s" % boundary] = h_values
            h5file["/input/q_%s" % boundary] = q_values

        
        # close file for avoiding catastrophies :-p
        h5file.close()
        
    def disable_pollock(self):
        if main_win.pollock.isEnabled():
            self.pollock.setDisabled(True)
        if main_win.plume.isEnabled():
            self.plume.setDisabled(True)
            
        if self.pollock.isChecked():
            self.pollock.setChecked(False)
            self.square.setChecked(True)
            self.view.selection_mode = "square"
            self.view.set_square()

        if self.plume.isChecked():
            self.plume.setChecked(False)
            self.square.setChecked(True)
            self.view.selection_mode = "square"
            self.view.set_square()
        
    def on_new(self):
        try: self.props.close()
        except AttributeError: pass
        self.win = New()
        self.win.show()
        
    def on_start(self):
        try: self.props.close()
        except AttributeError: pass
        self.disable_pollock()
        self.win = Start()
        self.win.show()
        
    def on_mesh(self):
        try: self.props.close()
        except AttributeError: pass
        self.disable_pollock()
        self.win = Mesh()
        self.win.show()
        
    def on_props(self):
        try: self.props.close()
        except AttributeError: pass
        try: self.win.close()
        except AttributeError: pass
        self.disable_pollock()
        self.props = Props()
        self.props.show()

        
    def on_boundaries(self):
        try: self.props.close()
        except AttributeError: pass
        self.disable_pollock()    
        self.win = Boundaries()
        QtGui.QMessageBox.information( self, "how to set boundary conditions.", "Select Boundary items ( the cells outside your domain or thick edges) \n and press enter to edit their properties" )

        
    def on_run(self):
        try: self.props.close()
        except AttributeError: pass
        self.win = Run()

    def on_visualize(self):  # Ben^#
        try: self.props.close()
        except AttributeError: pass
        self.win = Visu(self)  # Ben^#
        self.win.show()
        
    def set_scene(self, view): 
        if self.firststart != 0 :
            self.oldview.setParent(None)
        else:
            self.view.setParent(None)
            self.firststart += 1
                
        self.vbox.addWidget(view)
        self.oldview = view
    
    def update_legend(self):
        self.hbox_2.itemAt(1).widget().setParent(None)
        self.hbox_2.addWidget(self.legend)
#############################################################################################################

########## Global function to help pyinstaller to find a path
# def resource_path(relative_path):
#     """ Get absolute path to resource, works for dev and for PyInstaller """
#     try:
#         # PyInstaller creates a temp folder and stores path in _MEIPASS
# #         base_path = sys._MEIPASS
#         base_path = sys._MEI24402
#     except Exception:
#         base_path = os.path.abspath(".")
# 
#     return os.path.join(base_path, relative_path)

def resource_path(relative):
    if hasattr(sys, "_MEIPASS"):
        return os.path.join(sys._MEIPASS, relative)
    return os.path.join(relative)

############################################## Mr Main ######################################################
if __name__ == '__main__':
    try: app = QtGui.QApplication(sys.argv)
    except: app = QtGui.QApplication.instance()
    main_win = Window()
    main_win.show()
    sys.exit(app.exec_())

