#pollock's method, simple interactive example
import numpy as np
import matplotlib.pyplot as plt

class grid_pollock2D(object):
    def __init__(self,Lx,Ly,nx,ny):
         self.Lx, self.Ly = float(Lx), float(Ly)
         self.nx, self.ny = int(nx), int(ny)
         self.dx, self.dy = self.Lx/self.nx, self.Ly/self.ny
         self.output_string='Start point X (m); start point Y (m), end point X (m), end point Y (m), travel time (s)\n'
         self.output_string2='Clicked X (m), Clicked Y (m), Start X (m); Start Y (m), End X (m), End Y (m), Travel time (s)\n'
    def getCell(self,x,y):
        i=np.argmin(np.abs(np.linspace(self.Lx/2/self.nx,self.Lx-self.Lx/2/self.nx,self.nx,endpoint=True)-x))
        j=np.argmin(np.abs(np.linspace(self.Ly/2/self.ny,self.Ly-self.Ly/2/self.ny,self.ny,endpoint=True)-y))
        return i,j
    def sethphiK(self,h,phi,K):
        self.h, self.phi, self.K = h.T, phi.T, K.T#hydraulic head, porosity, hydraulic conductivity
    def setboundaries(self,h_bottom,q_bottom,h_top,q_top,h_left,q_left,h_right,q_right):
        self.h_bottom, self.q_bottom = h_bottom, -q_bottom
        self.h_top, self.q_top = h_top, q_top
        self.h_left, self.q_left = h_left, q_left
        self.h_right, self.q_right = h_right, -q_right
    def getv(self):
        dhx=(self.h[1:,:]-self.h[:-1,:])
        dhy=(self.h[:,1:]-self.h[:,:-1])
        Kx=0.5*(self.K[1:,:]+self.K[:-1,:])
        Ky=0.5*(self.K[:,1:]+self.K[:,:-1])
        phix=0.5*(self.phi[1:,:]+self.phi[:-1,:])
        phiy=0.5*(self.phi[:,1:]+self.phi[:,:-1])
        return -Kx/phix*dhx/self.dx, -Ky/phiy*dhy/self.dy
    def getv_extended(self):
        v_top = np.where(np.isnan(self.q_top),
                         2*(self.h_top-self.h[:,0])*self.K[:,0]/self.dx/self.phi[:,0],
                         self.q_top)
        v_bottom = np.where(np.isnan(self.q_bottom),
                         2*(self.h[:,-1]-self.h_bottom)*self.K[:,-1]/self.dx/self.phi[:,-1],
                         self.q_bottom)
        v_left = np.where(np.isnan(self.q_left),
                         2*(self.h_left-self.h[0,:])*self.K[0,:]/self.dx/self.phi[0,:],
                         self.q_left)
        v_right = np.where(np.isnan(self.q_right),
                         2*(self.h[-1,:]-self.h_right)*self.K[-1,:]/self.dx/self.phi[-1,:],
                         self.q_right)
        vx, vy = self.getv()
        vx = np.concatenate([[list(v_left)], vx, [list(v_right)]],axis=0)
        vy = np.concatenate([[list(v_top)],vy.T,[list(v_bottom)]],axis=0).T
        return vx, vy
    def pollock_grid(self,xp,yp,T_marker,backwards=False):
        i,j=self.getCell(xp,yp)
        X,Y,X_marker,Y_marker,dt0=[xp],[yp],[xp],[yp],0.
        while True:
            C=cell(i,j,self)
            try: dt0,i,j,xp,yp,x_smooth,y_smooth=C.pollock_cell(xp,yp,dt0,T_marker,
                                                                backwards)
            except: break
            X+=list(x_smooth)+[xp]
            Y+=list(y_smooth)+[yp]
        self.output_string+='%10.4e;%10.4e;%10.4e;%10.4e;%10.4e\n'%(X[0],Y[0],X[-1],Y[-1],dt0)
        return X,Y,X_marker,Y_marker
    def print_output(self,proj_name):
        f=open(proj_name+'_pathline.txt','w')
        f.write(self.output_string)
        f.close()
    def get_travel_times(self):
        lines=self.output_string.split('\n')[1:-1]#select the times from the output string
        times=np.array([float(item.split(';')[-1]) for item in lines]).reshape(len(lines)/2,2)
        return np.sum(times, axis=1)#sums the upstream and downstream time for each line

class cell(object):
    def __init__(self,i,j,grid):
        self.i, self.j = i, j
        self.grid=grid
        self.dx, self.dy = self.grid.dx, self.grid.dy#length and height
        self.x0, self.y0 = self.i*self.dx, self.j*self.dy#bottom left corner coordinates

    def pollock_cell(self,xp,yp,dt0,T_marker,backwards):
        vx,vy = self.grid.getv_extended()
        if backwards: vx, vy = -vx, -vy#to move back in time we do "v" -> "-v"
        i, j = self.i, self.j
        #test if we reached the edge
        if i==-1 or j==-1 or i==vx.shape[0] or j ==vy.shape[1]:
            return            
        #get the face velocities
        vx1, vx2 = vx[i,j], vx[i+1,j]
        vy1, vy2 = vy[i,j], vy[i,j+1]      
        #get the velocity gradients
        Ax=(vx2-vx1)/self.dx
        Ay=(vy2-vy1)/self.dy
        #get the initial velocity components
        vxp=vx1+Ax*(xp-self.x0)
        vyp=vy1+Ay*(yp-self.y0)
        #get the potential exit times to have the exit face
        #left face
        if vx1<0. and vxp<0. and Ax!=0.: dtx1=1/Ax*np.log(vx1/vxp)
        elif vx1<0. and Ax==0.: dtx1=(self.x0-xp)/vx1
        else: dtx1=float('inf')
        #right face
        if vx2>0. and vxp>0. and Ax!=0.: dtx2=1/Ax*np.log(vx2/vxp)
        elif vx2>0. and Ax==0.: dtx2=(self.x0+self.dx-xp)/vx2
        else: dtx2=float('inf')
        #bottom face    
        if vy1<0. and vyp<0. and Ay!=0.: dty1=1/Ay*np.log(vy1/vyp)
        elif vy1<0. and Ay==0.: dty1=(self.y0-yp)/vy1
        else: dty1=float('inf')
        #top face
        if vy2>0. and vyp>0. and Ay!=0.: dty2=1/Ay*np.log(vy2/vyp)
        elif vy2>0. and Ay==0.: dty2=(self.y0+self.dy-yp)/vy2
        else: dty2=float('inf')
        #get the actual exit time and exit face
        dt=min(dtx1,dtx2,dty1,dty2)
        if dt==dtx1: di,dj=-1,0
        elif dt==dtx2: di,dj=+1,0
        elif dt==dty1: di,dj=0,-1
        elif dt==dty2: di,dj=0,+1
        #get the exit point
        if Ax!=0.: xe=self.x0+1./Ax*(vxp*np.exp(Ax*dt)-vx1)
        else: xe=xp+dt*vx1
        if Ay!=0.: ye=self.y0+1./Ay*(vyp*np.exp(Ay*dt)-vy1)
        else: ye=yp+dt*vy1
        #we smoothen the curve by requesting 10 points per cell
        t_smooth=np.linspace(0.,dt,10,endpoint=False)
        if Ax!=0.: X_smooth=self.x0+1./Ax*(vxp*np.exp(Ax*t_smooth)-vx1)
        else: X_smooth=xp+t_smooth*vx1
        if Ay!=0.: Y_smooth=self.y0+1./Ay*(vyp*np.exp(Ay*t_smooth)-vy1)
        else: Y_smooth=yp+t_smooth*vy1
        dt0+=dt
        return dt0,i+di,j+dj,xe,ye,X_smooth,Y_smooth

# #test
# h = np.array([range(10) for i in range(5)], dtype=float)
# h_left = -0.5*np.ones((5,))
# h_right = 9.5*np.ones((5,))
# q_left = h_left*np.nan
# q_right = h_right*np.nan
# q_top = np.array([0.]*10)
# q_bottom = q_top
# h_top = np.nan*q_top
# h_bottom = h_top
# K = np.ones((5,10,),dtype=float)
# phi = np.array([range(1,11) for i in range(5)], dtype=float)
# 
# G=grid_pollock2D(200,100,10,5)
# G.sethphiK(h,phi,K)
# G.setboundaries(h_bottom,q_bottom,h_top,q_top,h_left,q_left,h_right,q_right)


# #test with a confined aquifer
# from script_confined_aquifer import *
# h=np.reshape(h_ss.value,(ny,nx))
# K=np.reshape(K.value,(ny,nx))
# phi=0.20*np.ones((ny,nx))
# G=grid_pollock2D(Lx,Ly,nx,ny)
# G.sethphiK(h,phi,K)
# 
# #create figure
# plt.ion()
# fig=plt.figure()
# ax=fig.add_subplot(111)
# 
# def onclick(event):#draws a streamline when a particle is clicked on the canvas
#     X,Y,X_marker,Y_marker=G.pollock_grid(event.xdata,event.ydata,10.*3600.)
#     plt.plot(X,Y,'k-')
#     plt.plot(X_marker,Y_marker,'ko')
#     plt.draw()
# 
# cid = fig.canvas.mpl_connect('button_press_event', onclick)
# 
# #plot streamlines, works only with matplotlib 1.2.x
# #plt.streamplot(np.linspace(Lx/2./nx,Lx-Lx/2./nx,nx,endpoint=True),
# #               np.linspace(Ly/2./ny,Ly-Ly/2./ny,ny,endpoint=True),
# #               np.reshape(-K.flatten()*h_ss.grad.value[0,:],(ny,nx)),
# #               np.reshape(-K.flatten()*h_ss.grad.value[1,:],(ny,nx)))
# plt.pcolor(np.linspace(0.,Lx,nx+1,endpoint=True),
#    np.linspace(0.,Ly,ny+1,endpoint=True),
#    np.reshape(K,(ny,nx)),
#    edgecolors='k')
# plt.contour(np.linspace(Lx/2./nx,Lx-Lx/2./nx,nx,endpoint=True),
#     np.linspace(Ly/2./ny,Ly-Ly/2./ny,ny,endpoint=True),
#     np.reshape(h_ss.value,(ny,nx)),
#     20)
# plt.gca().invert_yaxis()#to get the same convention as the gui
# 
# plt.draw()
